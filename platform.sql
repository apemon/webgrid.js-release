-- phpMyAdmin SQL Dump
-- version 3.5.2.2
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Apr 03, 2013 at 10:42 AM
-- Server version: 5.5.27
-- PHP Version: 5.4.7

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `webgrid`
--

--
-- Dumping data for table `platform`
--

INSERT INTO `platform` (`id`, `name`, `vendor`, `description`, `create_time`) VALUES
(1, 'chromium', 'Google, Inc.', 'open-source browser behind the Google Chrome browser', 1364927659005),
(2, 'chrome', 'Google, Inc.', 'Google Chrome web browser', 1364927659005),
(3, 'firefox', 'Mozilla', 'Mozilla Firefox', 1364927659005),
(4, 'safari', 'Apple', 'Apple Safari', 1364927659005),
(5, 'ie', 'Microsoft', 'Microsoft Internet Explorer', 1364927659005);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
