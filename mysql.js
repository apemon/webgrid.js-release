var mysql_info = {};
// configuration here
mysql_info['host'] = 'localhost'; // mysql host url
mysql_info['username'] = 'root'; // mysql username
mysql_info['password'] = ''; // mysql password
mysql_info['dbname'] = 'webgrid'; // mysql database name

module.exports = mysql_info;