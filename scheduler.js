// Record
var Record = require('./model/record');
// 3rd module
var express = require('express'),
		request = require('request');

// configuration
var port = process.argv[2] || 1234;
var workPool_url = 'http://localhost:2000';
var configuration_key = ['workpool_url'];

// configuration for mysql database
var mysql_info = require('./mysql');

// configuration for scheduler
var configuration;

// global variable
var index = 0;
var max_task = 300;
var job; // job description
var taskPool = {};  
var resultPool = []; 
var resultPool_size;
var ready = true;

var phy_paths;
var output_physical_path;
var file_service_url;

var session_timeout = 60*60*1000; // 1 hour in millisecond

var last_update;
// function
function init(){
	Record.init(mysql_info);
	Record.Configuration.listGet(configuration_key,function(err,results){
		if(err) return console.log(err);
		workPool_url = JSON.parse(results['workpool_url']);
		workPool_url = workPool_url[0];
		Record.setClientPool(30);
		Record.connect();
		reload();
	});
}

function reload(){
	index = 0;
	ready =false;
	configuration = {};
	resultPool = [];
	job = 'undefined';
	request(workPool_url+'/work/'+port, function (error, response, body) {
		if(error){
			setTimeout(reload,1*60000); // 1 minute
			return console.log(new Date().toISOString()+' : '+'can not connect to workpool '+workPool_url);
		} 

		if (!error && response.statusCode == 200) {
  		var temp_channel = JSON.parse(body);
  		if(temp_channel.status == 'error'){
  			console.log(new Date().toISOString()+' : '+temp_channel.data);
  			return setTimeout(reload,5*60000); // 5 minute 
  		}
  		temp_channel = JSON.parse(temp_channel.data);
  		job = temp_channel.job;
  		if(temp_channel.result){
  			resultPool = temp_channel.result;
  			resultPool_size = resultPool.length;
  		} else {
  			resultPool = [];
  			resultPool_size = 0;
  		}

  		// get file_service_url
  		if(resultPool_size > 0){
	  		file_service_url = resultPool[0].input_url;
	  		file_service_url = file_service_url.substring(0,file_service_url.lastIndexOf('/'));
	  		phy_paths = temp_channel.phy_paths;
  			output_physical_path = phy_paths[job.output_directory.substring(1)];  			
  		}
  		configuration = temp_channel.configuration;
  		console.log(new Date().toISOString()+' : '+'update scheduler from '+workPool_url);
  		last_update = new Date().getTime();
  		ready = true;
  		setTimeout(reload,30*60000); // reload scheduler every 30 minute
		}
	});
}

function mysqlTimestamp_toDate(timestamp){
	var t = timestamp.split(/[- :]/);
	return new Date(t[0], t[1]-1, t[2], t[3], t[4], t[5]).getTime();
}

function sql_timestamp(time){
	time = new Date(time);
	return time.getFullYear()+'-'+(time.getMonth()+1)+'-'+time.getDate()+' '+time.getHours()+':'+time.getMinutes()+':'+time.getSeconds();
}

// restful web service
var app = express.createServer();
var store = new express.session.MemoryStore;

app.configure(function(){
  delete express.bodyParser.parse['text/json'];
  app.use(express.bodyParser());
  app.use(express.cookieParser());
  app.use(express.session({store:store, cookie:{ httpOnly: true, maxAge: new Date().getTime()+60000}, secret:'webgrid'}));
  app.use(express.errorHandler({ dumpExceptions: true, showStack: true }));
  app.use(express.static(__dirname + '/client'));
  app.use(express.methodOverride());
  app.use(function(req,res,next){
		res.header("Access-Control-Allow-Origin", req.headers.origin || "*");
		res.header("Access-Control-Allow-Methods", "GET,POST,OPTIONS");
		res.header("Access-Control-Max-Age", 1728000);
		res.header("Access-Control-Allow-Headers", "content-Type,x-requested-with,origin,accept");
		res.header("Access-Control-Allow-Credentials", true);
		if(req.method == 'OPTIONS') res.send(200);
		else next();
	});
});

app.get('/',function(req,res){
	res.send('hello world');
});

app.get('/ping',function(req,res){
	res.send(''+new Date().getTime());
});

app.get('/task',function(req,res){
	var message = {};
	
	// check that job is valid and result pool not empty in non host enable mode
	// if host enable and result pool is empty. it mean that this scheduler serve only reserved result
	if(job == 'undefined' || (resultPool_size == 0 && configuration['host_enable'] == 'false')){
		message['status'] = 'error';
		message['data'] = 'no new task';
		return res.send(message);		
	}

	// check authen host
	if(configuration['host_enable']=='true' && typeof(req.session.host_id) == 'undefined'){
		message['status'] = 'error';
		message['data'] = 'unauthenticated host';
		return res.send(message);
	}

	// check status of scheduler module
	if(!ready){
		message['status'] = 'error';
		message['data'] = 'scheduler not ready';
		return res.send(message);
	}

	// serve result in host enable mode
	if(configuration['host_enable']=='true' && req.session.host_id){
		// check for cache
		if(!req.session.result){
			message['status'] = 'reauthen';
			message['data'] = 'reauthen required';
			return res.send(message);			
		}
		// check for reserved result
		var cache = req.session.result;
		if(req.query.result_id && cache[req.query.result_id]){
			var result_id = req.query.result_id;
			var client_serve = {};
			client_serve['job'] = job;
			client_serve['result'] = cache[result_id];
			message['status'] = 'success';
			message['data'] = client_serve;
			return res.send(message);			
		}
		// if it not in cache, serve normally and reserve it
		// check that result pool not empty
		if(resultPool_size == 0){
			message['status'] = 'error';
			message['data'] = 'no new task';
			return res.send(message);			
		}
		// if index > result pool length, reload it
		if(index >= resultPool_size){
			reload();
			message['status'] = 'error';
			message['data'] = 'scheduler out date';
			return res.send(message);
		}
		// serve result and reserved it
		var client_serve = {};
		client_serve['job'] = job;
		var temp_result = resultPool[index];
		var result_id = temp_result.id;
		var dispatch_time = new Date().getTime();
		temp_result['dispatch_time'] = dispatch_time
		index++;
		Record.Host.reserveResult(req.session.host_id,result_id,job.deadline_duration,dispatch_time,function(err,results){
			if(err){
				message['status'] = 'error';
				message['data'] = 'scheduler internal error';
				return res.send(message);
			}
			message['status'] = 'success';
			temp_result['deadline_time'] = results.deadline_time;
			req['session']['result'][result_id] = temp_result;
			client_serve['result'] = temp_result; 
			message['data'] = client_serve;
			return res.send(message); 
		});
	} else {
		// check that index has not larger than result pool size
		if(index >= resultPool_size){
			reload();
			message['status'] = 'error';
			message['data'] = 'scheduler out date';
			return res.send(message);			
		}
		var client_serve = {};
		client_serve['job'] = job;
		var temp_result = resultPool[index];
		// note that this not update dispatch time in database, so we trust that client not tamper it
		temp_result['dispatch_time'] = new Date().getTime();
		client_serve['result'] = temp_result;
		index++;
		message['status'] = 'success';
		message['data'] = client_serve;
		return res.send(message);
	} 
});

app.get('/login',function(req,res){
	console.log(req.session.id);
	return res.send(req.session.id);
});

app.post('/updatetime',function(req,res){
	var result_time = req.body;
	Record.Result.updateTime(result_time,function(err,results){
		var message = {};
		if(err){
			console.log(err);
			message['status'] = 'error';
			message['data'] = 'could not update time to server';
			return res.send(message);
		}
		message['status'] = 'success';
		message['data'] = 'result update';
		return res.send(message);
	});
});

app.post('/result',function(req,res){
	var new_result = req.body;
	new_result.status = 'fill';
	new_result['receive_time'] = new Date().getTime();
	new_result['user_agent'] = req.headers['user-agent'];
	new_result['remote_address'] = req.connection.remoteAddress;
	new_result['job_id'] = job.id;

	Record.Result.submit(new_result,output_physical_path,function(err,results){
		var message = {};

		if(err){
			console.log(err);
			message['status'] = 'error';
			message['data'] = err;
			return res.send(message);
		}
		if(results.affectedRows == 0){
			message['status'] = 'success';
			message['data'] = 'result receive, but got rejected for some reason';
			return res.send(message);
		} 
		message['status'] = 'success';
		message['data'] = 'result update';
		return res.send(message);
	});
});

// host register service
app.post('/host/register',function(req,res){
	var message = {};
	if(configuration['host_enable'] == 'false'){
		message['status'] = 'success';
		message['data'] = 'this project does not need to authen';
		return res.send(message);
	}
	var host_info = req.body;
	Record.Host.register(host_info,req.connection.remoteAddress,function(err,results){
		/*
			return message
				status: success or error
				data: if status is error, this data is error message
							if status is success, this data is host object consists of
							id,authen,platform
		*/
		if(err){
			message['status'] = 'error';
			message['data'] = err;
			return res.send(message);
		} else {
			message['status'] = 'success';
			message['data'] = results;
			return res.send(message);
		}
	});
});

// host authen service
app.post('/host/authen',function(req,res){
	var message = {};
	// check host_enable for this project
	if(configuration['host_enable'] == 'false'){
		message['status'] = 'success';
		message['data'] = 'this project does not need to authen';
		return res.send(message);
	}

	var host_data = req.body;
	// validator authen to prevent sql injection
	var authen_regex = /^[a-zA-Z0-9]{32}$/i;
	if(!authen_regex.test(host_data.authen)){
		message['status'] = 'fail';
		message['data'] = 'invalid authen format';
		return res.send(message);
	}
	// query for host
	Record.Host.authen(host_data.id,host_data.authen,function(err,results){
		if(err){
			message['status'] = 'error';
			message['data'] = err;
			return res.send(message);
		}
		// check if host found
		if(results.length == 0){
			message['status'] = 'not found';
			message['data'] = 'incorrect host data';
			return res.send(message);
		}
		// check if this host pass expire time
		var current_time = new Date().getTime();
		var expire_time = results[0].expire_time;
		if(current_time>expire_time){
			message['status'] = 'expire';
			message['data'] = 'the host information is expired';
			return res.send(message);
		}
		// register session here
		req.session.host_id = host_data.id;
		//req.session.cookie.maxAge = session_timeout;
		// finally update last_ip_addr and last_active_time 
		Record.Host.updateActiveTime(host_data.id,req.connection.remoteAddress,function(err2,results2){
			if(err2){
				message['status'] = 'error';
				message['data'] = err;
				return res.send(message);
			}

			// get reserved result for this host and store in scheduler cache
			Record.Host.getReservedResult(req.session.host_id,job.id,function(err3,results3){
				if(err3){
					message['status'] = 'error';
					message['data'] = err;
					return res.send(message);
				}

				// cache reserved result for this host
				req['session']['result'] = {};
				var reserved_resultId = [];
				for(var i=0;i<results3.length;i++){
					var temp = results3[i];
					// create input url
					temp['input_url'] = file_service_url+'/'+temp.input_path;
					delete temp['input_path'];
					req['session']['result'][temp.id] = temp;
					reserved_resultId.push(temp.id);
				}

				message['status'] = 'success';
				message['data'] = reserved_resultId;
				message['job'] = job;
				return res.send(message);
			});
		});
	});
});

app.post('/host/update',function(req,res){
	var raw_data = req.body;
	var host_data = raw_data.host_data;
	var host_info = raw_data.host_info;
	var authen_regex = /^[a-zA-Z0-9]{32}$/i;
	var message = {};
	if(!authen_regex.test(host_data.authen)){
		message['status'] = 'fail';
		message['data'] = 'invalid authen format';
		return res.send(message);
	}
	Record.Host.prolong(host_data.id,host_data.authen,host_info,req.connection.remoteAddress,function(err,results){
		if(err){
			message['status'] = 'error';
			message['data'] = err;
			return res.send(message);
		} 
		message['status'] = 'success';
		message['data'] = results;
		return res.send(message);
	});
});

// some helper function

// main program here

init();
//reload();
app.listen(port);