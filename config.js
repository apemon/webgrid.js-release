var configuration = {};
// configuration here
configuration['scheduler_url'] = ['http://localhost:1234'];
configuration['mount'] = [{
	logical_path:'/monin',
	physical_path:'G:/thesis work/fs/monin/',
	read_permission:true,
	write_permission:false,
	url:'http://localhost:8888'},{
	logical_path:'/monout',
	physical_path:'G:/thesis work/fs/monout/',
	read_permission:true,
	write_permission:false,
	url:'http://localhost:8888'},{
	logical_path:'/wordin',
	physical_path:'G:/thesis work/fs/input/',
	read_permission:true,
	write_permission:false,
	url:'http://localhost:8888'},{
	logical_path:'/wordout',
	physical_path:'G:/thesis work/fs/output/',
	read_permission:true,
	write_permission:false,
	url:'http://localhost:8888'},{
	logical_path:'/customin',
	physical_path:'G:/thesis work/fs/customin/',
	read_permission:true,
	write_permission:false,
	url:'http://localhost:8888'},{
	logical_path:'/customout',
	physical_path:'G:/thesis work/fs/customout/',
	read_permission:true,
	write_permission:true,
	url:'http://localhost:8888'},{
	logical_path:'/pi_in',
	physical_path:'G:/thesis work/fs/pi_in/',
	read_permission:true,
	write_permission:true,
	url:'http://localhost:8888'},{
	logical_path:'/pi_out',
	physical_path:'G:/thesis work/fs/pi_out/',
	read_permission:true,
	write_permission:true,
	url:'http://localhost:8888'},{
	logical_path:'/coin_in',
	physical_path:'G:/thesis work/fs/coin_in/',
	read_permission:true,
	write_permission:true,
	url:'http://localhost:8888'},{
	logical_path:'/coin_out',
	physical_path:'G:/thesis work/fs/coin_out/',
	read_permission:true,
	write_permission:true,
	url:'http://localhost:8888'}];
configuration['workpool_url'] = ['http://localhost:2000'];
configuration['host_enable'] = 'true';
configuration['intermediate_delete'] = 'true';
configuration['ktserver_url'] = ['http://192.168.1.30'];
configuration['ktserver_port'] = 1978;

module.exports = configuration;