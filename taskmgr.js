// record module
var Record = require('./model/record');
var kyoto = require('./model/kyoto');
var fs = require('fs');

// configuration for mysql database
var mysql_info = require('./mysql');

// other variable
var configuration_key = ['ktserver_port','ktserver_url','mount'];
var configuration = {};
var ready = true;
var job;
// function

// function 
function init(){
	Record.init(mysql_info);
	Record.Configuration.listGet(configuration_key,function(err,results){
		if(err) return console.log(err);
		var ktserver_url = results['ktserver_url'];
		var ktserver_port = results['ktserver_port'];
		configuration = results;
		if(typeof(ktserver_url) != 'undefined'){
			if(ktserver_url.indexOf('http://') != -1) ktserver_url = ktserver_url.substring(7);
			kyoto.init(ktserver_url,ktserver_port);
		}
		main();
	});
}

function main(){
	if(!ready) return setTimeout(main,30*60000); // 60 minutes
	ready = false;
	// get current job
	Record.Workflow.getCurrentJob(function(err,results){
		if(err){
			ready = true;
			console.log(err);
			return setTimeout(main,30*60000); // 60 minutes
		}
		// if no job
		if(!results.id){
			ready = true;
			console.log(new Date().toISOString()+' : no job');
			return setTimeout(main,30*60000);
		}
		job = results;
		if(job.status == 'create') return job_start();
		// count total task
		Record.Job.countTotalTask(job.id,function(err2,results2){
			if(err2){
				ready = true;
				console.log(err2);
				return setTimeout(main,30*60000);
			}
			job['total_task'] = results2[0].count;
			checkStatus();
		});
	});
}

function checkStatus(){
	Record.Job.countTask(job.id,function(err,results){
		if(err){
			ready = true;
			console.log(err);
			return setTimeout(main,30*60000);
		}
		var task_status = results;
		var complete_task = task_status['complete'];
		if(complete_task == job['total_task']){
			// begin write final output file (in partition data) and start next job
			if(job['output_type'] == 'partition'){
				// get physical path
				var mount = JSON.parse(configuration['mount']);
				var output_path = '';
				for(var i=0;i<mount.length;i++){
					var path = mount[i];
					if(path.logical_path == job.output_directory){
						output_path = path.physical_path;
						break;
					}
				}
				// write partition output file
				kyoto.writePartition(output_path,job.partition_number,function(err2,results2){
					if(err2){
						ready = true;
						console.log(err2);
						return setTimeout(main,30*60000);
					}
					// update job status
					return updateJobStatus();
				});
			} else return updateJobStatus();
		} else {
			ready = true;
			return setTimeout(main,30*60000);
		}
	});
}

function updateJobStatus(){
	Record.Job.finish(job.id,function(err,results){
		if(err){
			ready = true;
			console.log(err);
			return setTimeout(main,30*60000);
		}
		ready = true;
		console.log(new Date().toISOString()+' : job '+job.name+' complete');
		return main();
	});
}

function job_start(){
	// batch create task
	Record.Task.batchCreate(job,function(err,results){
		if(err){
			ready = true;
			console.log(err);
			return setTimeout(main,30*60000);
		} 
		Record.Result.batchCreate(job,function(err2,results2){
			if(err2){
				ready = true;
				console.log(err2);
				return setTimeout(main,30*60000);
			}
			// create program script
			var program_script = '';
			var program_filepath = '';
			if(job.type == 'mapreduce'){
				program_script += unescape(job.input_reader)+'\n';
				program_script += unescape(job.algorithm)+'\n';
				program_script += unescape(job.combiner)+'\n';
				program_filepath += 'client/mapreduce.js';
			} else {
				program_script += unescape(job.custom);
				program_filepath += 'client/custom.js';
			}
			fs.writeFileSync(program_filepath,program_script);
			Record.Job.changeStatus(job.id,'start',function(err3,results3){
				if(err3){
					ready = true;
					console.log(err3);
					return setTimeout(main,30*60000);
				}
			});
			console.log(new Date().toISOString()+' : job '+job.name+' start');
			ready = true;
			return setTimeout(main,30*60000);
		});
	});
}

console.log(new Date().toISOString()+' : Task Manager started');
init();
