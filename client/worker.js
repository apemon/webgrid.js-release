/*
	worker unit
*/

importScripts('murmurhash.js'); 
importScripts('md5-min.js');
importScripts('custom.js');

// import 3rd script here

//	variable here
var default_delay_time = 2000;
var delay_time = default_delay_time;
var task;

var start;

// global variable
var scheduler_url;
var algorithm;
var combiner;
var input_reader;
var sort_method;
var output_type;
var deadline;
var deadline_duration;
var checkpoint;
var checkpoint_interval;
var resultId = 0;

var index=0;
var total_time=0;
var startTime;
var stopTime;
var last_check=0;
var cpdata;
var result_time;
var host_id;
//	helper function
function get(url,callback){
	var req = new XMLHttpRequest();
	req.open('GET', url, true);
	req.setRequestHeader('Content-Type', 'text/plain');	
	req.onreadystatechange = function(event) {
		if(req.readyState == 4) {
			if(req.status == 200) {
				callback(req.responseText);
			} else {
				callback(req.statusText);
			}
		}
	};
	req.send(null);
}

function post(url,data,callback){
	var req = new XMLHttpRequest();
	req.open('POST', url, true);
	req.setRequestHeader('Content-Type', 'application/json');
	req.onreadystatechange = function(event) {
		if(req.readyState == 4) {
			if(req.status == 200) {
				callback(req.responseText);
			} else {
				callback(req.statusText);
			}
		}
	};
	req.send(data);
}

//	function
function init(){
	// initialize some variable here
	start = true;
	fetch_task();
}

function clear(){
	results = {};
	partition = {};
}

function fetch_task(){
	if(!start) return;

	var task_url = scheduler_url+'/task';
	if(resultId!=0) task_url = task_url+'?result_id='+resultId;
	var task_fetch_start = new Date().getTime();
	get(task_url,function(data){
		//clear();
		var task_fetch = new Date().getTime() - task_fetch_start;
		data = JSON.parse(data);
		if(data.status == 'error'){
			delay_time = delay_time * 2;
			return setTimeout('fetch_task()',delay_time);
		} else {
			var temp_data = data.data;
			result = temp_data.result;
			resultId = result.id;
			job = temp_data.job;
			result.task_fetch = task_fetch;
			if(!result.client_receive_time) result.client_receive_time = new Date().getTime();
			delay_time = default_delay_time;

			//if(job.combiner) combiner = eval('('+unescape(job.combiner)+')');
			//if(job.input_reader) input_reader = eval('('+unescape(job.input_reader)+')'); else input_reader = default_input_reader;
			//algorithm = eval('('+unescape(job.algorithm)+')');

			sort_method = job.sort_method;
			output_type = job.output_type;
			deadline = job.deadline;
			deadline_duration = job.deadline_duration;
			checkpoint = job.checkpoint;
			checkpoint_interval = job.checkpoint_interval;

			fetch_input();
		}
	});
}

function fetch_input(){
	var input_fetch_start = new Date().getTime();
	get(result.input_url,function(data){
		var input_fetch = new Date().getTime() - input_fetch_start;
		result.input_fetch = input_fetch;
		if(data == '' || data == 'Not Found'){
			delay_time = delay_time*2;
			return setTimeout('fetch_task()',delay_time);
		}
		startTime = new Date().getTime();
		execute(data);
	});
}

function output_send(results){

	stopTime = new Date().getTime();
	total_time = total_time + (stopTime - last_check);
	result.execution_time = total_time;
	result.output = results;
	result.client_send_time = new Date().getTime();

	result_time = {};
	result_time.id = result.id;
	result_time.secret = result.secret;
	result_time.host_id = host_id;
	result_time.result_send = new Date().getTime();
	post(scheduler_url+'/result',JSON.stringify(result),function(data){
		// send done message
		result_time.result_send = new Date().getTime() - result_time.result_send;
		update_time(result_time);
	});
}

function update_time(result_time){
	post(scheduler_url+'/updatetime',JSON.stringify(result_time),function(data){
		var message = {};
		message['cmd'] = 'done';
		self.postMessage(JSON.stringify(message));
		start = false;
		return;
	});
}

// default function

function save_checkpoint(cpobject){
	var checkpoint = {};
	checkpoint['data'] = cpobject;
	checkpoint['client_receive_time'] = result.client_receive_time;
	total_time = total_time + (new Date().getTime() - last_check);
	last_check = new Date().getTime();
	checkpoint['last_check'] = last_check;
	checkpoint['total_time'] = total_time;
	checkpoint['id'] = result.id;
	checkpoint['task_fetch'] = result.task_fetch;
	checkpoint['input_fetch'] = result.input_fetch;
	var message = {};
	message['cmd'] = 'checkpoint';
	message['data'] = checkpoint;
	return self.postMessage(JSON.stringify(message));
}

function load_checkpoint(){
	return cpdata;
}

function report(progress){
	var message = {};
	message['cmd'] = 'report';
	message['data'] = progress;
	message = JSON.stringify(message);
	return self.postMessage(message);
}

//	worker part
self.onmessage = function(event){
	var raw = JSON.parse(event.data);
	switch(raw.cmd){
		case 'start':
			init();
			break;
		case 'stop':
			start = false;
			break;
		case 'set':
			var checkpoint = raw.data;
			last_check = checkpoint.last_check;
			cpdata = checkpoint.data;
			total_time = checkpoint.total_time;
			resultId = checkpoint.id;
			scheduler_url = checkpoint.scheduler_url;
			host_id = checkpoint.host_id;
			// in future, must implement construction of partition data
			break;
		default:
			var message = {};
			message['cmd'] = 'Unknown command';
			message = JSON.stringify(message);
			self.postMessage(message);
	}
};

