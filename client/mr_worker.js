/*
	worker unit
*/

importScripts('murmurhash.js'); 
importScripts('mapreduce.js');

// import 3rd script here

//	variable here
var input_list;
var results = {};
var default_delay_time = 2000;
var delay_time = default_delay_time;
var task;

var start;

// global variable
var scheduler_url;
var algorithm;
var combiner;
var input_reader;
var sort_method;
var output_type;
var deadline;
var deadline_duration;
var checkpoint;
var checkpoint_interval;
var resultId = 0;
var host_id;
var result_time;

var index=0;
var total_time=0;
var startTime;
var stopTime;
var last_check=0;
var progress;

//	helper function
function get(url,callback){
	var req = new XMLHttpRequest();
	req.open('GET', url, true);
	req.setRequestHeader('Content-Type', 'text/plain');	
	req.onreadystatechange = function(event) {
		if(req.readyState == 4) {
			if(req.status == 200) {
				callback(req.responseText);
			} else {
				callback(req.statusText);
			}
		}
	};
	req.send(null);
}

function post(url,data,callback){
	var req = new XMLHttpRequest();
	req.open('POST', url, true);
	req.setRequestHeader('Content-Type', 'application/json');
	req.onreadystatechange = function(event) {
		if(req.readyState == 4) {
			if(req.status == 200) {
				callback(req.responseText);
			} else {
				callback(req.statusText);
			}
		}
	};
	req.send(data);
}

//	function
function init(){
	// initialize some variable here
	start = true;
	fetch_task();
}

function clear(){
	results = {};
	partition = {};
}

function fetch_task(){
	if(!start) return;

	var task_url = scheduler_url+'/task';
	if(resultId!=0) task_url = task_url+'?result_id='+resultId;
	var task_fetch_start = new Date().getTime();
	get(task_url,function(data){
		//clear();
		var task_fetch = new Date().getTime() - task_fetch_start;
		data = JSON.parse(data);
		if(data.status == 'error'){
			delay_time = delay_time * 2;
			return setTimeout('fetch_task()',delay_time);
		} else {
			var temp_data = data.data;
			result = temp_data.result;
			resultId = result.id;
			result.task_fetch = task_fetch;
			job = temp_data.job;
			if(!result.client_receive_time) result.client_receive_time = new Date().getTime();
			delay_time = default_delay_time;

			//if(job.combiner) combiner = eval('('+unescape(job.combiner)+')');
			//if(job.input_reader) input_reader = eval('('+unescape(job.input_reader)+')'); else input_reader = default_input_reader;
			//algorithm = eval('('+unescape(job.algorithm)+')');

			sort_method = job.sort_method;
			output_type = job.output_type;
			deadline = job.deadline;
			deadline_duration = job.deadline_duration;
			checkpoint = job.checkpoint;
			checkpoint_interval = job.checkpoint_interval;

			fetch_input();
		}
	});
}

function fetch_input(){
	var input_fetch_start = new Date().getTime();
	get(result.input_url,function(data){
		var input_fetch = new Date().getTime() - input_fetch_start;
		result.input_fetch = input_fetch;
		if(data == '' || data == 'Not Found'){
			delay_time = delay_time*2;
			return setTimeout('fetch_task()',delay_time);
		}
		startTime = new Date().getTime();
		input_list = input_reader(data);
		execute();
	});
}

function execute(){
	for(;index<input_list.length;index++){
		algorithm(input_list[index].key,input_list[index].value);
		if((index+1)%checkpoint_interval == 0) default_checkpoint();
	}
	output_send();
}

function output_send(){
	var data = results;
	if(sort_method != 'none') data = data_sort(results);
	if(combiner) data = combiner(data);
	if(output_type == 'partition') data = partitioning(data);

	stopTime = new Date().getTime();
	total_time = total_time + (stopTime - last_check);
	result.execution_time = total_time;
	result.output = data;
	result.client_send_time = new Date().getTime();

	result_time = {};
	result_time.id = result.id;
	result_time.secret = result.secret;
	result_time.host_id = host_id;
	result_time.result_send = new Date().getTime();
	post(scheduler_url+'/result',JSON.stringify(result),function(data){
		result_time.result_send = new Date().getTime() - result_time.result_send;
		update_time(result_time);
	});
}

function update_time(result_time){
	post(scheduler_url+'/updatetime',JSON.stringify(result_time),function(data){
		// send done message
		var message = {};
		message['cmd'] = 'done';
		self.postMessage(JSON.stringify(message));
		start = false;
		return;
		//return setTimeout('fetch_task()',delay_time);
	});
}

// default function

function default_checkpoint(){
	var checkpoint = {};
	checkpoint['result'] = results;
	checkpoint['progress'] = (index/input_list.length)*100;
	checkpoint['client_receive_time'] = result.client_receive_time;
	total_time = total_time + (new Date().getTime() - last_check);
	last_check = new Date().getTime();
	checkpoint['last_check'] = last_check;
	checkpoint['total_time'] = total_time;
	checkpoint['id'] = result.id;
	checkpoint['index'] = index+1;
	checkpoint['task_fetch'] = result.task_fetch;
	checkpoint['input_fetch'] = result.input_fetch;
	var message = {};
	message['cmd'] = 'checkpoint';
	message['data'] = checkpoint;
	self.postMessage(JSON.stringify(message));
}

function default_input_reader(input){
	var inputs = []; 
	input=JSON.parse(input); 
	for(var i=0;i<input.length;i++){ 
		var temp = input[i]; 
		var newinput = {}; 
		for(var x in temp){ 
			newinput.key = x; 
			newinput.value = temp[x]; 
		} 
		inputs.push(newinput); 
	} 
	return inputs;
}

function wordcount_input_reader(input){
	var inputs = [];	
	input = input.toLowerCase();
	input = input.split("\n");
	for (var i=0; i<input.length;i++){
		var newinput = {};
		newinput.key = "";
		newinput.value = input[i];
		inputs.push(newinput);
	}
	return inputs;
}

//combiner function: combine map output to reduce network traffic
//ex.['a',1],['b',1],['a',1] -> ['a',2],['b',1]
function wordcount_combiner(data){
	var datas = {};
	for(var x in data){
		datas[x] = 0;
		if(data[x].length>1){
			for(var i=0;i<data[x].length;i++){
				datas[x]+=data[x][i];
			}
		}
	}
	return datas;
}

function partitioning(data){
	var datas = {};

	for(var x in data){
		var partition_number = murmurhash3_32_gc(x,1234)%job.partition_number;
		if(!datas[partition_number]) datas[partition_number] = {};
		datas[partition_number][x] = data[x];
	}

	return datas;
}

function data_sort(data){
	var datas = {};
	var temp = [];
	for(var x in data){
		var t = {};
		t.key = x;
		t.value = data[x];
		temp.push(t);
	}
	if(sort_method == 'alpha_asc'){
		temp.sort(function(a,b){
			if(a.key>b.key) return 1;
			else if(a.key<b.key) return -1;
			else return 0;
		});
	} else if(sort_method == 'alpha_dec'){
		temp.sort(function(a,b){
			if(a.key>b.key) return -1;
			else if(a.key<b.key) return 1;
			else return 0;
		});
	} else if(sort_method == 'num_asc'){
		temp.sort(function(a,b){
			return a.key-b.key;
		}); 
	} else if(sort_method == 'num_dec'){
		temp.sort(function(a,b){
			return b.key-a.key;
		});
	}
	for(var i=0;i<temp.length;i++){
		datas[temp[i].key] = temp[i].value;
	}	
	return datas;
}

function emit(key,value){
	if(!results[key]){
		results[key] = [];
		//if(output_type == 'partition') partition[key] = murmurhash3_32_gc(key,1234)%job.partition_number;
	} 
	results[key].push(value);
}

//	worker part
self.onmessage = function(event){
	var raw = JSON.parse(event.data);
	switch(raw.cmd){
		case 'start':
			init();
			break;
		case 'stop':
			start = false;
			break;
		case 'set':
			var checkpoint = raw.data;
			last_check = checkpoint.last_check;
			results = checkpoint.result;
			progress = checkpoint.progress;
			total_time = checkpoint.total_time;
			resultId = checkpoint.id;
			scheduler_url = checkpoint.scheduler_url;
			index = checkpoint.index;
			// in future, must implement construction of partition data
			break;
		default:
			var message = {};
			message['cmd'] = 'Unknown command';
			message = JSON.stringify(message);
			self.postMessage(message);
	}
};

