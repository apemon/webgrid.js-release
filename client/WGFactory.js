//WGFactory

function WGFactory(scheduler_url,job,number,resultIdList,host_data){
	var _this = this;
	if(!resultIdList) resultIdList = [];
	this.workers = [];
	this.resultIdList = resultIdList;
	this.taskQueue = resultIdList;
	this.worker_num = number;
	this.host_data = host_data;
	this.job = job;
	this.option = {};
	this.option['job_type'] = job.type;
	this.option['checkpoint'] = job.checkpoint;
	this.option['checkpoint_interval'] = job.checkpoint_interval;
	this.option['scheduler_url'] = scheduler_url;
	this.status = 'ready';
	// create worker
	for(var i=0;i<number;i++){
		var worker = new WGWorker(this,this.option);
		worker.setId(i);
		this.workers.push(worker);
	}
	
	this.nextTask = function(worker){
		if(_this.status != 'start') return;
		// first remove the finish result id and clean checkpoint
		//var result_id = worker.task.id; // must define it latter
		var worker_id = worker.id;
		var result_id = worker.resultId;
		_this.resultIdList.splice(_this.resultIdList.indexOf(result_id),1);
		var next_task = '';
		// clean checkpoint here
		if(_this.taskQueue.length > 0) next_task = _this.taskQueue.shift();

		var checkpoint = {};
		if(next_task){
			// first load checkpoint from localstorage
			if(localStorage.getItem('cp'+next_task)) checkpoint = JSON.parse(localStorage.getItem('cp'+next_task));
			else {
				// create new checkpoint
				checkpoint['id'] = next_task;
				checkpoint['progress'] = 0;
				checkpoint['total_time'] = 0;
				checkpoint['last_check'] = new Date().getTime();
				checkpoint['client_receive_time'] = 0;
				checkpoint['index'] = 0;
				checkpoint['host_id'] = _this.host_data.id;
				checkpoint['result'] = {};
				localStorage.setItem('cp'+next_task,JSON.stringify(checkpoint));
			}
		} else {
			checkpoint['id'] = 0;
			checkpoint['progress'] = 0;
			checkpoint['total_time'] = 0;
			checkpoint['last_check'] = new Date().getTime();
			checkpoint['client_receive_time'] = 0;
			checkpoint['index'] = 0;
			checkpoint['host_id'] = _this.host_data.id;
			checkpoint['result'] = {};
		}
		checkpoint['scheduler_url'] = _this.option['scheduler_url'];
		_this.workers[worker_id].set(checkpoint);
		_this.workers[worker_id].start();
	}

}

// add new worker
WGFactory.prototype.add = function(){

};

// remove worker
WGFactory.prototype.remove = function(index){

};

// return status of the factory and each worker
WGFactory.prototype.report = function(){

}

WGFactory.prototype.start = function(){
	this.status = 'start';
	for(var i=0;i<this.workers.length;i++){
		var next_task = '';
		if(this.taskQueue.length > 0) next_task = this.taskQueue.shift();
		var checkpoint = {};
		if(next_task){
			// first load checkpoint from localstorage
			if(localStorage.getItem('cp'+next_task)) checkpoint = JSON.parse(localStorage.getItem('cp'+next_task));
			else {
				// create new checkpoint
				checkpoint['id'] = next_task;
				checkpoint['progress'] = 0;
				checkpoint['total_time'] = 0;
				checkpoint['last_check'] = new Date().getTime();
				checkpoint['client_receive_time'] = 0;
				checkpoint['index'] = 0;
				checkpoint['host_id'] = this.host_data.id;
				checkpoint['result'] = {};
				localStorage.setItem('cp'+next_task,JSON.stringify(checkpoint));
			}
		} else {
			checkpoint['id'] = 0;
			checkpoint['progress'] = 0;
			checkpoint['total_time'] = 0;
			checkpoint['last_check'] = new Date().getTime();
			checkpoint['client_receive_time'] = 0;
			checkpoint['index'] = 0;
			checkpoint['host_id'] = this.host_data.id
			checkpoint['result'] = {};			
		}
		checkpoint['scheduler_url'] = this.option['scheduler_url'];
		this.workers[i].set(checkpoint);
		this.workers[i].start();
	}
}

WGFactory.prototype.stop = function(){
	this.status='ready';
	for(var i=0;i<this.workers.length;i++){
		this.workers[i].stop();
	} 
}