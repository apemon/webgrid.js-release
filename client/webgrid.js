// global variable
var worker_num = 1;
var scheduler_url;
var host_enable = true;
var factory;

/**
	browser_detect
	return browser = {
		browser: browser name
		vendor: vendor name
		platform: platform name (etc. Win32)
		version: browser version
		user_agent: raw userAgent string
	}
**/
function browser_detect(){
	var browser_object = {};
	var user_agent = navigator.userAgent;
	browser_object['user_agent'] = user_agent;
	browser_object['timezone_offset'] = new Date().getTimezoneOffset();
	// check major web browser
	var index;

	// chromium
	// sample userAgent string:
	// Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/535.19 (KHTML, like Gecko) Ubuntu/11.10 Chromium/18.0.1025.142 Chrome/18.0.1025.142 Safari/535.19
	index = user_agent.indexOf('Chromium');
	if(index != -1){
		browser_object['browser'] = 'chromium';
		browser_object['vendor'] = navigator.vendor;
		browser_object['platform'] = navigator.platform;
		// get version
		var temp = user_agent.substring(index);
		temp = temp.split(' ');
		temp = temp[0];
		var version = temp.substring(temp.indexOf('/')+1);
		browser_object['version'] = version;
		return browser_object;
	}

	// google chrome
	// sample userAgent string:
	// Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.4 (KHTML, like Gecko) Chrome/22.0.1229.94 Safari/537.4
	index = user_agent.indexOf('Chrome');
	if(index != -1){
		browser_object['browser'] = 'chrome';
		browser_object['vendor'] = navigator.vendor;
		browser_object['platform'] = navigator.platform;
		// get version
		var temp = user_agent.substring(index);
		temp = temp.split(' ');
		temp = temp[0];
		var version = temp.substring(temp.indexOf('/')+1);
		browser_object['version'] = version;	
		return browser_object;
	}

	// firefox
	// sample userAgent string:
	// Mozilla/5.0 (Windows NT 6.1; rv:15.0) Gecko/20100101 Firefox/15.0.1
	index = user_agent.indexOf('Firefox');
	if(index != -1){
		browser_object['browser'] = 'firefox';
		browser_object['vendor'] = 'Mozilla';
		browser_object['platform'] = navigator.platform;
		// get version
		var temp = user_agent.substring(index);
		temp = temp.split(' ');
		temp = temp[0];
		var version = temp.substring(temp.indexOf('/')+1);
		browser_object['version'] = version;	
		return browser_object;
	}

	// Safari
	// sample userAgent string:
	// Mozilla/5.0 (Macintosh; Intel Mac OS X 10_6_8) AppleWebKit/537.13+ (KHTML, like Gecko) Version/5.1.7 Safari/534.57.2
	var vendor = navigator.vendor;
	if(vendor) index = vendor.indexOf('Apple');
	if(index != -1){
		browser_object['browser'] = 'safari';
		browser_object['vendor'] = vendor;
		browser_object['platform'] = navigator.platform;
		// get version
		index = user_agent.indexOf('Version');
		var temp = user_agent.substring(index);
		temp = temp.split(' ');
		temp = temp[0];
		var version = temp.substring(temp.indexOf('/')+1);
		browser_object['version'] = version;	
		return browser_object;
	}

	// Microsoft Internet Explorer
	// sample userAgent string:
	// Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.1; Trident/5.0; SLCC2; .NET CLR 2.0.50727; .NET CLR 3.5.30729; .NET CLR 3.0.30729; Media Center PC 6.0; .NET4.0C; BRI/2; Tablet PC 2.0; .NET4.0E)
	index = user_agent.indexOf('MSIE');
	if(index != -1){
		browser_object['browser'] = 'ie';
		browser_object['vendor'] = 'Microsoft';
		browser_object['platform'] = navigator.platform;
		// get version
		var temp = user_agent.substring(index);
		temp = temp.split(' ');
		temp = temp[1];
		var version = temp.substring(0,temp.length-1);
		browser_object['version'] = version;	
		return browser_object;
	}	
}

/**
	benchmark(foo): 
	return GigaFLOPS
**/
function benchmark(foo){
	var start = new Date().getTime();
	var x = 3.14159*foo;
    var i;
    for (i=0; i<500000000; i++) {
        x += 5.12313123;
        x *= 0.5398394834;
   }
  var stop = new Date().getTime();
  var total_time = stop - start;
  return 1E9*1E3/total_time;
}

/**
	init function
**/
function init(scheduler_url){
	// check if browser support localStorage
	var localStorage_support = true;
	if(typeof(localStorage) == 'undefined') localStorage_support = false;
	// check for registered hosts
	var host_data = localStorage.getItem('webgrid');
	if(host_data == null) host_register(scheduler_url);
	else host_authen(scheduler_url,host_data);
}

function host_authen(scheduler_url,host_data){
	post(scheduler_url+'/host/authen',host_data,function(res){
		var message = JSON.parse(res);
		if(message.status == 'fail') return console.log(res);
		else if(message.status == 'not found'){
			localStorage.clear();
			host_register(scheduler_url);
		} else if(message.status == 'error'){
			// authen in next 5 minute
			return setTimeout(host_authen,5*60000);
		} else if(message.status == 'expire'){
			return host_update(scheduler_url,host_data);
		} else if(message.status == 'reauthen'){
			return host_authen(scheduler_url,host_data);
		} else if(message.status == 'success'){
			// if host not need to authen
			if(typeof(message.data) == 'string'){
				// implement logic for non authen host

			} else {
				res = JSON.parse(res);
				host_data = JSON.parse(host_data);
				// clear old checkpoint
				var reserved_task = res.data;
				var keys = Object.keys(localStorage);
				for(var i=0;i<keys.length;i++){
					if(keys[i] == 'webgrid') continue;
					var found = false;
					for(var j=0;j<reserved_task.length;j++){
						if(keys[i] == 'cp'+reserved_task[j]){
							found = true;
							break;
						}
					}
					if(!found) localStorage.removeItem(keys[i]);
				}
				factory = new WGFactory(scheduler_url,res.job,worker_num,res.data,host_data);
				//factory.start();
				// first time , spawn new worker  
				//spawn_worker(1,'');
				//var resultIdList = message.data;
				//if(resultIdList.length == 0) spawn_worker(worker_num);
				// var factory = new WGFactory(worker_num);
				// var factory = new WGFactory(worker_num,resultIdList);
			}
		}
	});
}

function host_register(scheduler_url){
	// get browser information
	var browser_object = browser_detect();
	// perform benchmark to estimate FLOPS
	var FLOPS = benchmark(1.00);
	// register to server
	var host_info = {};
	host_info['browser_info'] = browser_object;
	host_info['flops'] = FLOPS;
	host_info = JSON.stringify(host_info);
	post(scheduler_url+'/host/register',host_info,function(res){
		res = JSON.parse(res);
		//if(res.status == 'error') return setTimeout();
		if(res.status == 'error'){
			console.log(res.data);
			return setTimeout(init(scheduler_url),10000);
		} else {
			if(typeof(res.data) == 'string') host_enable = false;
			else {
				var host_data = res.data;
				// save host data in localStorage
				host_data = JSON.stringify(host_data);
				localStorage.setItem('webgrid',host_data);
				return host_authen(scheduler_url,host_data);
			}
		}
	});
}

function host_update(scheduler_url,host_data){
	// get browser information
	var browser_object = browser_detect();
	// perform benchmark to estimate FLOPS
	var FLOPS = benchmark(1.00);
	// register to server
	var update_host= {};
	var host_info = {};
	host_info['browser_info'] = browser_object;
	host_info['flops'] = FLOPS;
	update_host['host_info'] = host_info;
	update_host['host_data'] = JSON.parse(host_data);
	update_host = JSON.stringify(update_host);
	post(scheduler_url+'/host/update',update_host,function(res){
		res = JSON.parse(res);
		// set new authen to localStorage
		var new_host_data = JSON.stringify(res.data);
		localStorage.setItem('webgrid',new_host_data);
		return host_authen(scheduler_url,new_host_data);
	});	
}

/*
// spawn normal worker
// new worker function : spawn_worker(parameter)
function spawn_worker(num,data){
	var data = JSON.parse(data);
	for(var i=0;i<num;i++){
		if(data.result_id[i]) var worker = new WGWorker(data[0]);
	}
}*/


//	helper function
function get(url,callback){
	var req = new XMLHttpRequest();
	req.open('GET', url, true);
	req.setRequestHeader('Content-Type', 'text/plain');
	req.withCredentials = true;	
	req.onreadystatechange = function(event) {
		if(req.readyState == 4) {
			if(req.status == 200) {
				callback(req.responseText);
			} else {
				callback(req.statusText);
			}
		}
	};
	req.send(null);
}

function post(url,data,callback){
	var req = new XMLHttpRequest();
	req.open('POST', url, true);
	req.setRequestHeader('Content-Type', 'application/json');
	req.withCredentials = true;
	req.onreadystatechange = function(event) {
		if(req.readyState == 4) {
			if(req.status == 200) {
				callback(req.responseText);
			} else {
				callback(req.statusText);
			}
		}
	};
	req.send(data);
} 

