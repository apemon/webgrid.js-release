// WGWorker 
// checkpoint contain data for worker to resume task
function WGWorker(parentFactory,option){
	var _this = this;
	var worker;
	if(option.job_type == 'mapreduce') worker = new Worker('mr_worker.js');
	else worker = new Worker('worker.js');
	this.id = 0;
	this.progress = 0;
	this.create_time = new Date().getTime();
	this.done = 0;
	this.status='ready';
	this.last_check = new Date().getTime();
	this.resultId = 0;
	this.parentFactory = parentFactory;
	worker.onmessage = function(event) {
		var raw = JSON.parse(event.data);
		switch(raw.cmd){
			case 'done':
				_this.done++;
				_this.progress=0;
				// clean checkpoint
				localStorage.removeItem('cp'+_this.resultId);
				_this.parentFactory.nextTask(_this);
				break;
			case 'report':_this.progress=raw.data;break;
			case 'checkpoint':
				var checkpoint = raw.data;
				// set worker property
				_this.resultId = checkpoint.id;
				_this.last_check = checkpoint.last_check;
				localStorage.setItem('cp'+checkpoint.id,JSON.stringify(checkpoint));
				break;
			case 'status':_this.status=raw.data;break;
			default: console.log(event.data);
		}
	};
	this.worker = worker;
}

WGWorker.prototype.start = function(){
	var message = {};
	message['cmd'] = 'start';
	message = JSON.stringify(message);
	this.worker.postMessage(message);
}

WGWorker.prototype.stop = function(){
	var message = {};
	message['cmd'] = 'stop';
	message = JSON.stringify(message);
	this.worker.postMessage(message);
}

WGWorker.prototype.set = function(checkpoint){
	this.progress = checkpoint.progress;
	checkpoint.last_check = new Date().getTime();
	this.last_check = checkpoint.last_check;
	this.total_time = checkpoint.total_time;
	this.resultId = checkpoint.id;
	var message = {};
	message['cmd'] = 'set';
	message['data'] = checkpoint;
	this.worker.postMessage(JSON.stringify(message));
}

WGWorker.prototype.report = function(){
	return this.status;
}

WGWorker.prototype.setId = function(id){
	this.id = id;
}
//