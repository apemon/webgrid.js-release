// Record
var Record = require('./model/record');
// 3rd module
var express = require('express');

// configuration (to be implement in configuration db)
var port = process.argv[2] || 2000;
var max_taskPool_size = 10000;
var max_resultPool_size = 300000;
// configuration for mysql database
var mysql_info = require('./mysql');

// scheduler list
var scheduler;

// mount list
var mount;
var phy_paths;

// configuration list
var configuration;

// some local configuration
var job_select_field = ['id','name','type','input_directory','output_directory','sort_method','output_type','partition_number','checkpoint','checkpoint_interval','deadline','deadline_duration'];
var task_select_field = ['id','input_path'];
var result_select_field = ['id','deadline_time','host_id','task_id','secret'];
var configuration_key = ['scheduler_url','host_enable'];

// global variable
var job; // job description
var channel = {}; 

var resultPool_size;
var taskPool_size; 

var ready = true;
// function
function init(){
	configuration = {};
	Record.init(mysql_info);
	Record.Configuration.listGet(configuration_key,function(err,results){
		if(err) return console.log(err);
		// host enable data
		configuration['host_enable'] = results.host_enable;

		// scheduler data
		scheduler = JSON.parse(results.scheduler_url);

		Record.Mount.list(function(err2,results2){
			if(err2) return console.log(err);
			var mount_temp = JSON.parse(results2.mount);
			delete results2;
			mount = {};
			phy_paths = {};
			for(var i=0;i<mount_temp.length;i++){
				if(!mount[mount_temp[i].logical_path]){
					mount[mount_temp[i].logical_path.substring(1)] = mount_temp[i].url;
					phy_paths[mount_temp[i].logical_path.substring(1)] = mount_temp[i].physical_path;
				}
			}
			beginFetchTask();
		});
	});
}

function updateResult(job_id){
	Record.Result.updateDeadline(job_id,function(err,results){
		if(err) return console.log(err);
		return;
	});
}

function beginFetchTask(){
	ready = false;
	var taskPool = {};
	var resultPool = [];

	channel = {};

	// fetch job description
	Record.Job.getbyStatus('start',job_select_field,function(err,results){
		if(err) return console.log(err);
		if(results.length == 0) return console.log('no more job to work');
		job = results[0];

		delete results;
		delete err;

		updateResult(job.id);
		
		// fetch task
		Record.Task.rangeQuerybyJobIdStatus(job.id,'create',task_select_field,0,max_taskPool_size,function(err2,results2){
			if(err2) return console.log(err2);
			if(results2.length == 0){
				ready = true;
				// pack data only job for scheduler
				for(var i=0;i<scheduler.length;i++){
					var scheduler_port = scheduler[i].substr(scheduler[i].lastIndexOf(':')+1);
					channel[scheduler_port] = {};
					channel[scheduler_port]['job'] = job;
					channel[scheduler_port]['configuration'] = configuration;
					channel[scheduler_port]['phy_paths'] = phy_paths;
				}
				return setTimeout(beginFetchTask,10*60000); // 10 minute
			} 
			var taskIdList = [];
			for(var i=0;i<results2.length;i++){
				taskPool[results2[i].id] = results2[i];
				taskIdList.push(results2[i].id);
			}

			delete results2;
			delete err2;
			// fetch result
			Record.Result.getbyTaskIdList(taskIdList,'create',result_select_field,function(err3,results3){
				if(err3) return console.log(err3);
				if(results3.length==0){
					ready = true;
					// pack data only job for scheduler
					for(var i=0;i<scheduler.length;i++){
						var scheduler_port = scheduler[i].substr(scheduler[i].lastIndexOf(':')+1);
						channel[scheduler_port] = {};
						channel[scheduler_port]['job'] = job;
						channel[scheduler_port]['configuration'] = configuration;
						channel[scheduler_port]['phy_paths'] = phy_paths;
					}
					return setTimeout(beginFetchTask,10*60000); // 10 minute
				} 

				resultPool = arrayShuffle(results3);
				delete results3;
				delete err3;
				resultPool_size = resultPool.length;

				if(resultPool_size > max_resultPool_size) resultPool_size = max_resultPool_size;
				var scheduler_resultPool_size = Math.ceil(resultPool_size/scheduler.length);

				// packing data for result
				var job_input_url = mount[job.input_directory.substring(1)]+job.input_directory+'/';
				for(var i=0;i<resultPool_size;i++){
					resultPool[i].input_url = job_input_url+taskPool[resultPool[i].task_id].input_path;
				}

				// dispatch result,task,job for each scheduler
				var begin = 0;
				var end = 0;
				for(var i=0;i<scheduler.length;i++){
					var scheduler_port = scheduler[i].substr(scheduler[i].lastIndexOf(':')+1);
					channel[scheduler_port] = {};
					channel[scheduler_port]['job'] = job;
					begin = i*scheduler_resultPool_size;
					end = begin + scheduler_resultPool_size;
					var temp_resultPool = resultPool.slice(begin,end);
					channel[scheduler_port]['result'] = temp_resultPool;
					channel[scheduler_port]['configuration'] = configuration;
					channel[scheduler_port]['phy_paths'] = phy_paths;
				}
				ready = true;
				console.log(new Date().toISOString()+' : '+'update data');
				return setTimeout(beginFetchTask,10*60000); // 10 minute
			});
		});
	});
}

// helper function
function arrayShuffle(oldArray) {
	var newArray = oldArray.slice();
 	var len = newArray.length;
	var i = len;
	 while (i--) {
	 	var p = parseInt(Math.random()*len);
		var t = newArray[i];
  		newArray[i] = newArray[p];
	  	newArray[p] = t;
 	}
	return newArray; 
}

// restful web service
var app = express.createServer();

app.get('/',function(req,res){
	res.send('hello world');
});

app.get('/work/:port',function(req,res){
	res.header("Access-Control-Allow-Origin", "*");
	res.header("Access-Control-Allow-Headers", "X-Requested-With");
	var message = {};
	if(!ready){
		message['status'] = 'error';
		message['data'] = 'workpool is not ready yet';
	} else {
		message['status'] = 'success';
		message['data'] = JSON.stringify(channel[req.params.port]);
	}
	return res.send(message); 
});

init();
app.listen(port);