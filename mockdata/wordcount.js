// Record
var Record = require('../model/record');
var mysql_info = require('../mysql');
//
var job = require('../model/job').job;

var wordcount_map = new job({
	id:1,
	name:'map',
	//name:'Monte Carlo Integration',
	description:'wordcount',
	input_directory:'/wordin',
	output_directory:'/wordout',
	sort_method:'alpha_asc',
	output_type:'partition',
	max_result:5,
	majority:3,
	type:'mapreduce',
	validation_type:'checksum',
	checkpoint: true,
	checkpoint_interval: 1000,
	deadline : true,
	partition_number: 5,
	input_reader:"function input_reader(input){ var inputs = []; input = input.toLowerCase(); input = input.replace('\\r',''); input = input.split('\\n'); for(var i=0;i<input.length;i++){ var newinput = {}; newinput.key = ''; newinput.value = input[i]; inputs.push(newinput);} return inputs; }",
	algorithm:"function algorithm(key,value){ var words = value.split(' ');for(var i=0;i<words.length;i++){emit(words[i],1);}}",
	combiner:"function combiner(data){ var datas = {}; for(var x in data){ datas[x] = 0; if(data[x].length>=1){ for(var i=0;i<data[x].length;i++){ datas[x]+=data[x][i]; }}} return datas; }"
});

var wordcount_reduce = new job({
	id:1,
	name:'reduce',
	//name:'Monte Carlo Integration',
	description:'wordcount',
	input_directory:'/wordout',
	output_directory:'/wordout',
	sort_method:'alpha_asc',
	output_type:'store',
	max_result:5,
	majority:3,
	type:'mapreduce',
	validation_type:'checksum',
	checkpoint: true,
	checkpoint_interval: 1000,
	deadline : true,
	input_reader:"function input_reader(input){ var inputs = []; input = input.toLowerCase(); input = input.replace('\\r',''); input = input.split('\\n'); for(var i=0;i<input.length;i++){ var newinput = {}; var temp = input[i].split('\\t');newinput.key = temp[0]; newinput.value = temp[1]; inputs.push(newinput);} return inputs; }",
	algorithm:"function algorithm(key,value){ if(key == '' || value == '') return; var temp = value.split(','); var sum = 0; for(var i=0;i<temp.length;i++){ sum += parseInt(temp[i],10); } emit(key,sum); }",
	combiner:"function combiner(data){ var datas = {}; for(var x in data){ datas[x] = 0; if(data[x].length>=1){ for(var i=0;i<data[x].length;i++){ datas[x]+=data[x][i]; }}} return datas; }"
});

Record.init(mysql_info);
Record.Job.insert(wordcount_map,function(err,results){
	if(err) return;
	Record.Job.insert(wordcount_reduce,function(err2,results2){
		if(err2) return;
		Record.Workflow.connect(1,2,function(err3,results3){
			if(err3) return;
			return console.log(results3);
		});
	});
});