var fs = require('fs');

var line = process.argv[2];
var num = process.argv[3];
var file_name = process.argv[4];

var index = 0;
var words;
var total;

function main(){
	fs.readFile('word.txt',function(err,data){
		if(err) return console.log('file not found');
		words = data.toString();
		words = words.split('\n');
		total = words.length;
		gen_text();
	});
}
	
function gen_text(){
    var data = "";
	for(var i=0;i<line;i++){
	  var text="";
	  //generate random text per line
	  var n = Math.round(Math.random()*10000)%12;
	  if(n < 4) n=4;
		for(var j=0;j<n;j++){
			var r = Math.round(Math.random()*100000)%total;
			text += words[r]+' ';
		}
		if(i==line-1) data+=text;
		else data+=text+'\n';
	}
	create_file(index,data,function(err){
		if(err) return console.log(err);
		index++;
		if(index >= num) return;
		gen_text();
	});
}

function create_file(i,content,callback){
	fs.writeFile(file_name+i,content,function(err){
		if(err) return callback(err);
		else return callback('');
	});
}

main();