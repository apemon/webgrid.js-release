// Record
var Record = require('../model/record');
var mysql_info = require('../mysql');
var Mock = require('./mock');
var config = require('../config');

// initial platform
var platforms = [];
var platform = {};

platform['name'] = 'chromium';
platform['vendor'] = 'Google, Inc.';
platform['description'] = 'open-source browser behind the Google Chrome browser';
platforms.push(platform);

platform = {};
platform['name'] = 'chrome';
platform['vendor'] = 'Google, Inc.';
platform['description'] = 'Google Chrome web browser';
platforms.push(platform);

platform = {};
platform['name'] = 'firefox';
platform['vendor'] = 'Mozilla';
platform['description'] = 'Mozilla Firefox';
platforms.push(platform);

platform = {};
platform['name'] = 'safari';
platform['vendor'] = 'Apple';
platform['description'] = 'Apple Safari';
platforms.push(platform);

platform = {};
platform['name'] = 'ie';
platform['vendor'] = 'Microsoft';
platform['description'] = 'Microsoft Internet Explorer';
platforms.push(platform);

Mock.init(mysql_info);
Mock.reset(function(err,results){
	if(err) return console.log(err);
	Record.Platform.batchInsert(platforms,function(err2,results2){
		if(err2) return console.log(err2);
		var result = {};
		result['reset'] = results;
		result['platform'] = results2;
		console.log(result);
		load_configuration();
	});
});

function load_configuration(){
	var conf = {};
	conf['scheduler_url'] = JSON.stringify(config.scheduler_url);
	conf['mount'] = JSON.stringify(config.mount);
	conf['workpool_url'] = JSON.stringify(config.workpool_url);
	conf['host_enable'] = config.host_enable;
	conf['intermediate_delete'] = config.intermediate_delete;
	conf['ktserver_url'] = config.ktserver_url;
	conf['ktserver_port'] = config.ktserver_port;
	Record.Configuration.listSet(conf,function(err,results){
		if(err) return err;
		return results;
	});
}