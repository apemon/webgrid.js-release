// Record
var Record = require('../model/record');
var mysql_info = require('../mysql');
//
var job = require('../model/job').job;
var task = require('../model/task').task;

var superpi = new job({
	name:'superpi',
	description:'superpi calculator with monte carlo algorithm',
	input_directory:'/pi_in',
	output_directory:'/pi_out',
	sort_method:'none',
	output_type:'store',
	max_result:3,
	majority:1,
	type:'custom',
	validation_type:'custom',
	error_rate:1,
	checkpoint: true,
	deadline : true,
	deadline_duration: 1800000,
	custom:'function execute(e){var t=e;var n=0;var r=0;var i=load_checkpoint();if(typeof i!="undefined"){r=i.index;n=i.hit}for(;r<t;r++){if(r%1e8==0){i={};i["hit"]=n;i["index"]=r;save_checkpoint(i);report(r/t*100)}x=Math.random();y=Math.random();d=Math.pow(x,2)+Math.pow(y,2);if(d<=1)n++}output_send(n)}'
});

Record.init(mysql_info);
Record.Job.insert(superpi,function(err,results){
	if(err) return;
	// create batch work
	Record.Task.createWork('superpi','in',2000,function(err,results){
		Record.Job.start('superpi');
	});
	// start job
});