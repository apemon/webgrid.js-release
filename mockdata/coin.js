// Record
var Record = require('../model/record');
var mysql_info = require('../mysql');
//
var job = require('../model/job').job;

var coin = new job({
	name:'coin',
	//name:'Monte Carlo Integration',
	description:'evolutionary computation with coin',
	input_directory:'/coin_in',
	output_directory:'/coin_out',
	output_type:'store',
	max_result:5,
	majority:3,
	type:'custom',
	validation_type:'custom',
	checkpoint: true,
	deadline : true,
	custom: 'function execute(e){var t=0;var n=parseTSP(e);var r=n.generation;var i=new Coin(n);var s=load_checkpoint();if(typeof s!="undefined"){t=s.index;i.load(JSON.parse(s.coin))}for(;t<r;t++){if(t%100==0){s={};s["index"]=t;s["coin"]=i.stringify();save_checkpoint(s);report(t/r*100)}i.nextGen()}output_send(i.outputObject())}importScripts("tspparser.js");importScripts("coinjs.js")'
});

Record.init(mysql_info);
Record.Job.insert(coin,function(err,results){
	if(err) return;
	return console.log(results);
});