/*
	this is fileservice which open port for static file
*/
var Record = require('./model/record');

// configuration
var path="";
var port=process.argv[2] || 8888;

// configuration for mysql database
var mysql_info = require('./mysql');

var fs = require('fs');

var express = require('express');

// global variable
var mount;

app = express.createServer();

app.configure(function(){
    app.use(express.methodOverride());
    app.use(express.bodyParser());
    app.use(app.router);
    app.use(express.errorHandler({ dumpExceptions: true, showStack: true }));
});
 
app.get('/', function(req, res){
  console.log(__dirname);
  res.send('hello world');
});

function main(){
	read_mount_path();
}

function read_mount_path(){
	Record.init(mysql_info);
	Record.Mount.list(function(err,results){
		if(err) return console.log(err);
		mount = JSON.parse(results.mount);
		write_mount_file(function(err2){
			if(err2){
				return console.log(err2);
			}
			require('./mount')(app);
			app.listen(port);
		});
	});
}

function write_mount_file(callback){
	var data = 'module.exports = function(app){\n\t';
	for(var i=0;i<mount.length;i++){
		data += 'app.get(\''+mount[i].logical_path+'/:filename\', function(req, res){\n\t\t';
		data += 'res.header("Access-Control-Allow-Origin", "*");\n\t\t';
		data += 'res.header("Access-Control-Allow-Headers", "X-Requested-With");\n\t\t';
		data += 'res.sendfile("'+mount[i].physical_path+'"+req.params.filename);\n\t';
		data += '});\n\t';
	}
	data += '}';

	fs.writeFile(__dirname+'/mount.js',data,function(err){
		if(err) return callback(err);
		else return callback('');
	});
}

main();