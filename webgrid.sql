-- phpMyAdmin SQL Dump
-- version 3.5.2.2
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Apr 03, 2013 at 10:41 AM
-- Server version: 5.5.27
-- PHP Version: 5.4.7

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `webgrid`
--

-- --------------------------------------------------------

--
-- Table structure for table `configuration`
--

CREATE TABLE IF NOT EXISTS `configuration` (
  `name` varchar(256) NOT NULL,
  `data` text NOT NULL,
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `host`
--

CREATE TABLE IF NOT EXISTS `host` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `authen` varchar(32) NOT NULL,
  `platform` int(11) NOT NULL,
  `version` varchar(100) NOT NULL,
  `timezone_offset` int(11) NOT NULL,
  `last_ip_addr` varchar(15) NOT NULL,
  `create_time` bigint(11) NOT NULL,
  `expire_time` bigint(11) NOT NULL,
  `last_mod_time` bigint(11) NOT NULL,
  `last_active_time` bigint(11) NOT NULL,
  `os_platform` varchar(100) NOT NULL,
  `flops` double NOT NULL,
  `user_agent` varchar(250) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `authen` (`authen`),
  KEY `os_platform` (`os_platform`),
  KEY `timezone_offset` (`timezone_offset`),
  KEY `platform` (`platform`),
  KEY `version` (`version`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

-- --------------------------------------------------------

--
-- Table structure for table `job`
--

CREATE TABLE IF NOT EXISTS `job` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(250) NOT NULL,
  `description` text NOT NULL,
  `type` varchar(25) NOT NULL,
  `status` varchar(10) NOT NULL,
  `create_time` bigint(20) DEFAULT NULL,
  `last_mod_time` bigint(20) NOT NULL,
  `complete_time` bigint(20) DEFAULT NULL,
  `input_directory` varchar(512) NOT NULL,
  `output_directory` varchar(512) NOT NULL,
  `sort_method` varchar(10) NOT NULL,
  `output_type` varchar(10) NOT NULL,
  `partition_number` int(11) NOT NULL,
  `validation_type` varchar(10) NOT NULL,
  `error_rate` float NOT NULL,
  `max_result` int(11) NOT NULL,
  `majority` int(11) NOT NULL,
  `checkpoint` tinyint(1) NOT NULL,
  `checkpoint_interval` int(11) NOT NULL,
  `deadline` tinyint(4) NOT NULL,
  `deadline_duration` int(11) NOT NULL,
  `input_reader` text NOT NULL,
  `algorithm` text NOT NULL,
  `combiner` text NOT NULL,
  `custom` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `name` (`name`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

-- --------------------------------------------------------

--
-- Table structure for table `partition`
--

CREATE TABLE IF NOT EXISTS `partition` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `data_key` varchar(250) NOT NULL,
  `partition_num` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `key` (`data_key`),
  KEY `partition` (`partition_num`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `platform`
--

CREATE TABLE IF NOT EXISTS `platform` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(250) NOT NULL,
  `vendor` varchar(100) NOT NULL,
  `description` text NOT NULL,
  `create_time` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`),
  KEY `vendor` (`vendor`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

-- --------------------------------------------------------

--
-- Table structure for table `result`
--

CREATE TABLE IF NOT EXISTS `result` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `status` varchar(10) NOT NULL,
  `create_time` bigint(20) NOT NULL,
  `dispatch_time` bigint(20) NOT NULL,
  `execution_time` bigint(20) NOT NULL,
  `receive_time` bigint(20) NOT NULL,
  `client_send_time` bigint(20) NOT NULL,
  `client_receive_time` bigint(20) NOT NULL,
  `deadline_time` bigint(20) NOT NULL,
  `task_fetch` int(11) NOT NULL,
  `input_fetch` int(11) NOT NULL,
  `result_send` int(11) NOT NULL,
  `secret` varchar(32) NOT NULL,
  `user_agent` varchar(1024) NOT NULL,
  `remote_address` varchar(512) NOT NULL,
  `host_id` int(11) NOT NULL,
  `task_id` int(11) NOT NULL,
  `job_id` int(11) NOT NULL,
  `output_path` varchar(500) NOT NULL,
  `output_checksum` varchar(500) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `job_id` (`job_id`),
  KEY `task_id` (`task_id`),
  KEY `status` (`status`,`task_id`),
  KEY `secret` (`secret`),
  KEY `host_id` (`host_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=301 ;

-- --------------------------------------------------------

--
-- Table structure for table `task`
--

CREATE TABLE IF NOT EXISTS `task` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `status` varchar(10) NOT NULL,
  `create_time` bigint(20) NOT NULL,
  `validate_time` bigint(20) DEFAULT NULL,
  `file_write_time` bigint(20) NOT NULL,
  `partition_time` bigint(20) NOT NULL,
  `complete_time` bigint(20) NOT NULL,
  `last_mod_time` bigint(20) NOT NULL,
  `job_id` int(11) NOT NULL,
  `candidate_result` int(11) NOT NULL,
  `input_path` varchar(512) NOT NULL,
  `output_checksum` varchar(32) NOT NULL,
  `output_path` varchar(512) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `status` (`status`,`job_id`),
  KEY `validate_time` (`validate_time`),
  KEY `create_time` (`create_time`),
  KEY `job_id` (`job_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=101 ;

-- --------------------------------------------------------

--
-- Table structure for table `workflow`
--

CREATE TABLE IF NOT EXISTS `workflow` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `source` int(11) NOT NULL,
  `destination` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
