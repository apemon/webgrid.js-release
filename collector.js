// Record 
var Record = require('./model/record');
var kyoto = require('./model/kyoto');

// other include
var fs = require('fs'),
		crypto = require('crypto');

// mysql configuration
var mysql_info = require('./mysql');

// custom collector
var custom_collector = require('./custom_collector');

// configuration
var job_select_field = ['id','input_directory','output_directory','output_type','validation_type'];
var configuration_key = ['intermediate_delete','ktserver_url','ktserver_port']; 
// global variable
var job;

var need_collectTask;

var intermediate_delete;

// function 
function init(){
	//kyoto.init('161.200.92.217',1978);
	Record.Configuration.listGet(configuration_key,function(err,results){
		if(err) return console.log(err);
		intermediate_delete = results['intermediate_delete'];
		var ktserver_url = results['ktserver_url'];
		var ktserver_port = results['ktserver_port'];
		if(typeof(ktserver_url) != 'undefined'){
			if(ktserver_url.indexOf('http://') != -1) ktserver_url = ktserver_url.substring(7);
			kyoto.init(ktserver_url,ktserver_port);
		}
		main();
	});
}

function main(){
	// get running job
	Record.Job.getbyStatus('start',job_select_field,function(err,results){
		if(err){
			ready = true;
			console.log(err);
			return setTimeout(main,10*60000); // 10 minute
		}
		job = results[0];
		// get all validate task
		Record.Task.getByJobIdStatus(job.id,'validated',function(err2,results2){
			if(err2){
				ready = true;
				console.log(err);
				return setTimeout(main,10*60000); // 10 minute
			}
			need_collectTask = results2;
			if(job.output_type == 'store') collectTask_store(0);
			else if(job.output_type == 'partition') collectTask_partition(0);
		});
	});
}

function collectTask_store(index){
	if(index >= need_collectTask.length){
		ready = true;
		return setTimeout(main,10*60000); // 10 minute
	}
	var task_id = need_collectTask[index].id;
	if(job.validation_type == 'avg'){
			// get all accept result from taskId
			Record.Result.getAVGAcceptResult(task_id,function(err,results){
				if(err){
					ready = true;
					console.log(err);
					return setTimeout(main,10*60000);
				}
				// organize output data
				var outputList = results.data;
				var total = 0;
				var result_id_index = {};
				var raw_result = {};
				for(var x in outputList){
					var temp = JSON.parse(outputList[x]);
					result_id_index[total] = x;
					total++;
					for(var y in temp){
						if(!raw_result[y]) raw_result[y] = [];
						raw_result[y].push(temp[y]);
					}
				}
				// find average value
				var final_result = {};
				for(var x in raw_result){
					var sum = 0;
					for(var i=0;i<raw_result[x].length;i++){
						sum += raw_result[x][i];
					}
					var avg = sum/raw_result[x].length;
					final_result[x] = avg;
				}
				var output_path = results.results[0].output_path;
				output_path = output_path.substring(0,output_path.lastIndexOf('/')+1);
				output_path += job.id+'_'+task_id+'_'+'out';
				var output = '';
				// construct output file content
				for(var x in final_result){
					output+=x+' : '+final_result[x]+'\n';
				}
				// find checksum
				var output_checksum = crypto.createHash('md5').update(unescape(output)).digest('hex');
				// write final output file
				fs.writeFile(output_path,output,function(err2){
					if(err2){
						ready = true;
						console.log(err3);
						return setTimeout(main,10*60000); // 10 minute
					}

					var file_write_time = new Date().getTime();
					Record.Task.complete(task_id,output_path,output_checksum,file_write_time,function(err3,results3){
						if(err3){
							ready = true;
							console.log(err2);
							return setTimeout(main,10*60000); // 10 minute
						}

						console.log(new Date().toISOString()+' : collect task id '+task_id+' complete --> file:'+output_path);
						if(intermediate_delete == 'true') delete_intermediate(index,task_id);
						else {
							index++;
							return collectTask_store(index);
						}
					});
				});
			});
	} else if(job.validation_type == 'checksum'){
		Record.Result.getChecksumAcceptResult(task_id,function(err,results){
			if(err){
				ready = true;
				console.log(err);
				return setTimeout(main,10*60000);
			}
			var final_result = JSON.parse(results.data.data);
			// organize output data
			var output_path = results.results[0].output_path;
			output_path = output_path.substring(0,output_path.lastIndexOf('/')+1);
			output_path += job.id+'_'+task_id+'_'+'out';
			var output = '';
			// construct output file
			for(var x in final_result){
				output += x+' : '+final_result[x]+'\n';
			}
			// find checksum
			var output_checksum = crypto.createHash('md5').update(unescape(output)).digest('hex');
			// write final output file
			fs.writeFile(output_path,output,function(err2){
				if(err2){
					ready = true;
					console.log(err2);
					return setTimeout(main,10*60000); // 10 minute
				}

				var file_write_time = new Date().getTime();
				Record.Task.complete(task_id,output_path,output_checksum,file_write_time,function(err3,results3){
					if(err3){
						ready = true;
						console.log(err3);
						return setTimeout(main,10*60000); // 10 minutes
					}
					console.log(new Date().toISOString()+' : collect task id '+task_id+' complete --> file:'+output_path);
					if(intermediate_delete == 'true') delete_intermediate(index,task_id);	
					else {
						index++;
						return collectTask_store(index);
					}					
				});		
			});
		});	
	} else if(job.validation_type == 'custom'){
		// get some accept result
		Record.Result.getAcceptResults(task_id,function(err,results){
			if(err){
				ready = true;
				console.log(err);
				return setTimeout(main,10*60000);
			}
			// send the data to custom collector
			// format array[{data:'',result:object},...]
			// return the final result content
			var final_result = custom_collector.collect(results);
			// save the final result
			var output_path = results[0].result.output_path;
			output_path = output_path.substring(0,output_path.lastIndexOf('/')+1);
			output_path += job.id+'_'+task_id+'_'+'out';
			// write file
			fs.writeFile(output_path,final_result,function(err2){
				if(err2){
					ready = true;
					console.log(err2);
					return setTimeout(main,10*60000); // 10 minute
				}
				// create file checksum
				var output_checksum = crypto.createHash('md5').update(unescape(final_result)).digest('hex');
				var file_write_time = new Date().getTime();
				// update task status (change from 'validated' to 'complete')
				Record.Task.complete(task_id,output_path,output_checksum,file_write_time,function(err3,results3){
					if(err3){
						ready = true;
						console.log(err3);
						return setTimeout(main,10*60000); // 10 minutes
					}
					console.log(new Date().toISOString()+' : collect task id '+task_id+' complete --> file:'+output_path);
					// delete intermediate file 
					if(intermediate_delete == 'true') delete_intermediate(index,task_id);	
					else {
						index++;
						return collectTask_store(index);
					}
				});
			});
		});
	}
}

// to be implement in future
function collectTask_partition(index){
	if(index >= need_collectTask.length){
		ready = true;
		return setTimeout(main,30*60000); // 30 minute
	}
	var task_id = need_collectTask[index].id;
	
	if(job.validation_type == 'avg'){
		// implement for avg validation type here
	} else if(job.validation_type == 'checksum'){
		Record.Result.getChecksumAcceptResult(task_id,function(err,results){
			if(err){
				ready = true;
				console.log(err);
				return setTimeout(main,10*60000);
			}
			var final_result = JSON.parse(results.data.data);
			kyoto.partition(final_result,function(err2,results2){
				if(err2){
					ready = true;
					console.log(err2);
					return setTimeout(main,10*60000);
				}
				Record.Task.partition(task_id,function(err3,results3){
					if(err3){
						ready = true;
						console.log(err3);
						return setTimeout(main,10*60000);
					}
					console.log(new Date().toISOString()+' : collect task id '+task_id+' complete --> key-value database');
					if(intermediate_delete == 'true') delete_intermediatePartition(index,task_id);
					else {
						index++;
						return collectTask_partition(index);
					}
				});
			});
		});
	}
}

function delete_intermediate(index,taskId){
	// get all file path 
	Record.Result.getOutputPathbyTaskId(taskId,function(err,fileList){
		if(err){
			ready = true;
			console.log(err);
			return setTimeout(main,10*60000); // 10 minute
		}
		// 
		var count = 1;
		var total = fileList.length;
		for(var i=0;i<fileList.length;i++){
			var file = fileList[i].output_path;
			delete_file(file,function(err2){
				if(count == total){
					console.log(new Date().toISOString()+' : delete task id '+taskId+' intermediate file complete');
					// remove result output path
					Record.Result.removeOutput(taskId,function(err2,results2){
						if(err2){
							ready = true;
							console.log(err2);
							return setTimeout(main,10*60000); // 10 minute
						}
						index++;
						return collectTask_store(index);
					});
				} else count++;
			});
		}
	});
}

function delete_intermediatePartition(index,taskId){
	// get all file path 
	Record.Result.getOutputPathbyTaskId(taskId,function(err,fileList){
		if(err){
			ready = true;
			console.log(err);
			return setTimeout(main,10*60000); // 10 minute
		}
		// 
		var count = 1;
		var total = fileList.length;
		for(var i=0;i<fileList.length;i++){
			var file = fileList[i].output_path;
			delete_file(file,function(err2){
				if(count == total){
					console.log(new Date().toISOString()+' : delete task id '+taskId+' intermediate file complete');
					// remove result output path
					Record.Result.removeOutput(taskId,function(err2,results2){
						if(err2){
							ready = true;
							console.log(err2);
							return setTimeout(main,10*60000); // 10 minute
						}
						index++;
						return collectTask_partition(index);
					});
				} else count++;
			});
		}
	});
}

function delete_file(file,callback){
	if(!callback) callback = function(){};

	fs.unlink(file,function(err){
		if(err) return callback(err);
		return callback('');
	});
}

Record.init(mysql_info);
init();