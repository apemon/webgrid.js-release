// Record
var Record = require('./model/record');

// mysql configuration
var mysql_info = require('./mysql');

// include
var crypto = require('crypto');

// custom validator
var custom_validator = require('./custom_validator');
// global variable
var job;

var majority;
var max_result;
var validation_type;
var error_rate;

var need_validateTaskId = [];
var ready = true;

var job_select_field = ['id','output_directory','output_type','validation_type','error_rate','max_result','majority'];
// function
function main(){
	if(!ready) return setTimeout(main,10*60000); // 10 minute
	need_validateTaskId = [];

	// get job first
	Record.Job.getbyStatus('start',job_select_field,function(err,results){
		if(err){
			ready = true;
			console.log(err);
			return setTimeout(main,10*60000); // 10 minute
		} 

		job = results[0];
		if(job.validation_type == 'none'){
			ready = true;
			return setTimeout(main,10*60000); // 10 minute
		}

		majority = job.majority;
		max_result = job.max_result;
		validation_type = job.validation_type;
		error_rate = job.error_rate;

		// get count result 
		Record.Result.getResultCount(job.id,'fill',function(err2,results2){
			if(err2){
				ready = true;
				console.log(new Date().toISOString()+' : '+err2);
				return setTimeout(main,10*60000); // 10 minute
			}

			// iterate array to check majority
			for(var i=0;i<results2.length;i++){
				if(results2[i].count >= majority) need_validateTaskId.push(results2[i]);
			}

			if(need_validateTaskId.length==0){
				ready = true;
				console.log(new Date().toISOString()+' : '+'no result to validate');
				return setTimeout(main,10*60000); // 10 minute
			}
			validateResult(0);
		});
	});
}

function validateResult(index){
	if(index>=need_validateTaskId.length){
		ready = true;
		return setTimeout(main,10*60000); // 10 minute
	} 

	var task_id = need_validateTaskId[index].task_id;
	// get result list
	Record.Result.getFillResult(task_id,function(err,results){
		if(err){
			ready = true;
			return console.log(err);
		}
		var validate_result = validate(results.data);
		if(validate_result.status){
			// update result and task status
			var acceptResultIdList = [];
			var rejectResultIdList = [];
			for(var x in validate_result.result){
				if(validate_result.result[x] == 'accept') acceptResultIdList.push(x);
				else rejectResultIdList.push(x);
			}
			Record.Task.updateValidateTaskId(task_id,acceptResultIdList,rejectResultIdList,function(err2,results2){
				if(err2) return console.log(err2);
				index++;
				console.log(new Date().toISOString()+' : validate task id '+task_id+' complete --> accept');
				return validateResult(index);
			});
		} else {
			if(validate_result.count < max_result){
				// create more result up to max_result
				Record.Result.createbyJobTaskList(job.id,task_id,job.max_result-job.majority,function(err2,results2){
					if(err2) return console.log(err2);
					index++;
					console.log(new Date().toISOString()+' : validate task id '+task_id+' complete --> reject, create more results');
					return validateResult(index);
				});
			} else {
				// bury task
				Record.Task.bury(task_id,function(err2,results2){
					if(err2) return console.log(err2);
					index++;
					console.log(new Date().toISOString()+' : validate task id '+task_id+' complete --> reject, burying task');
					return validateResult(index);
				});
			}
		}
		ready = true;
		//return setTimeout(main,30*60000); // 30 minute
	});
}

// outputList = [{id:id,output:output},...]
// return (findCandidate,[id1:accept,id2:reject,id3:accept,...]) // findCandidate true/false
function validate(outputList){
	if(validation_type == 'checksum'){
		// list of  checksum
		var checksums = {};
		var checksumCount = {};
		var max = -1;
		var max_checksum;
		var total = 0;
		for(var x in outputList){
			total++;
			checksums[x] = crypto.createHash('md5').update(unescape(outputList[x])).digest('hex');
			if(!checksumCount[checksums[x]]) checksumCount[checksums[x]] = 0;
			checksumCount[checksums[x]]++;
			if(checksumCount[checksums[x]] >= max){
				max = checksumCount[checksums[x]];
				max_checksum = checksums[x];
			}
		}
		var validation_result = {};
		if(max < majority){
			validation_result['status'] = false;
			validation_result['count'] = total;
		} else {
			var acceptList = {};
			for(var x in checksums){
				if(checksums[x] == max_checksum) acceptList[x] = 'accept';
				else acceptList[x] = 'reject';
			}
			validation_result['status'] = true;
			validation_result['result'] = acceptList;
		}
		return validation_result;
	} else if(validation_type == 'avg'){
		var raw_result = {};
		var total=0;
		var validation_matrix = [];
		var result_id_index = {};

		for(var x in outputList){
			result_id_index[total] = x;
			var temp = JSON.parse(outputList[x]);
			total++;
			for(var y in temp){
				if(!raw_result[y]){
					raw_result[y] = [];
				}
				raw_result[y].push(temp[y]);
			}
		}
		for(var x in raw_result){
			// find average
			var sum = 0;
			for(var i=0;i<raw_result[x].length;i++){
				sum+=raw_result[x][i];
			}
			var avg = sum/raw_result[x].length;
			
			// find SD
			var sum_diff = 0;
			for(var i=0;i<raw_result[x].length;i++){
				sum_diff += Math.pow(raw_result[x][i] - avg,2);
			}
			var sd = Math.sqrt(sum_diff/(raw_result[x].length-1));

			// construct matrix
			var matrix = [];
			var accept_range = error_rate * sd;
			for(var i=0;i<raw_result[x].length;i++){
				if(Math.abs(raw_result[x][i]-avg) <= accept_range) matrix.push(1);
				else matrix.push(0);
			}
			validation_matrix.push(matrix);
		}

		var acceptList = {};
		var acceptCount = 0;
		for(var i=0;i<validation_matrix[0].length;i++){
			var check = true;
			for(var j=0;j<validation_matrix.length;j++){
				if(validation_matrix[j][i]==0){
					check = false;
					break;
				}
			}
			if(check){
				acceptList[result_id_index[i]] = 'accept';
				acceptCount++;
			} else acceptList[result_id_index[i]] = 'reject';
		}
		var validation_result = {};
		if(acceptCount >= majority){
			validation_result['status'] = true;
			validation_result['result'] = acceptList;
		} else {
			validation_result['status'] = false;
			validation_result['count'] = total;
		}
		return validation_result;
	} // implement your own validation method here
	else if(validation_type == 'debug'){
		var acceptList = {};
		for(var x in outputList){
			acceptList[x] = 'accept';
		}
		var validation_result = {};
		validation_result['status'] = true;
		validation_result['result'] = acceptList;
		return validation_result;
	} else if(validation_type == 'custom'){
		return custom_validator.validate(outputList);
	}
}

Record.init(mysql_info);
Record.setClientPool(10);
Record.connect();
main();