exports.validate = function(outputList){
	// return true only
	var validation_result = {};
	var accept_list = {};
	for(var x in outputList){
		accept_list[x] = 'accept';
	}
	validation_result['status'] = true;
	validation_result['result'] = accept_list;
	return validation_result;
}