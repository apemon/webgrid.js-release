var URL = require('url');
var http = require('http');
var fs = require('fs');
var request = require('request');

var Record = require('./record');
var mysql_info = require('../mysql');

var Kyoto = {};

// 	method:'POST',
// 	uri:'http://192.168.1.30:1978/rpc/set',
// 	headers:{
// 		'content-type': 'text/tab-separated-values'
// 	}
module.exports = Kyoto;

Kyoto.init = function(host,port){
	if(!port) port = 1978;
	uri = URL.parse('');
	uri.protocol = 'http:';
	uri.port = port;
	uri.hostname = host;
	Record.init(mysql_info);
}

Kyoto.get = function(key,db,callback){
	if(!callback) callback = function(){};
	// encode to URI
	key = encodeURIComponent(key);
	var url = URL.format(uri)+'/rpc/get';
	url += '?DB='+db+'&key='+key;
	request(url,function(err,res,body){
		if(err) return callback(err);
		var result = body.split('\t');
		result = result[1];
		result = result.replace('\n','');
		// decode result
		result = decodeURIComponent(result);
		return callback('',result);
	});
}

Kyoto.set = function(key,value,db,callback){
	if(!callback) callback = function(){};
	// encode to URI
	key = encodeURIComponent(key);
	value = encodeURIComponent(value);
	var url = URL.format(uri)+'/rpc/set';
	var content = 'DB\t'+db+'\n';
	content += 'key\t'+key+'\nvalue\t'+value+'\n';
	request({
		method:'POST',
		uri:url,
		headers:{
			'content-type': 'text/tab-separated-values; colenc=U'
		},
		body:content
	},function(err,res,body){
		if(err) return callback(err);
		return callback('',body);
	});
}

Kyoto.getBulk = getBulk;

function getBulk(keys,db,callback){
	if(!callback) callback = function(){};
	// encode all keys
	var url = URL.format(uri)+'/rpc/get_bulk';
	var content = 'atomic\t\n';
	content += 'DB\t'+db+'\n';
	for(var i=0;i<keys.length;i++){
		content += '_'+encodeURIComponent(keys[i])+'\t\n';
	}
	request({
		method:'POST',
		uri:url,
		headers:{
			'content-type': 'text/tab-separated-values; colenc=U'
		},
		body:content
	},function(err,res,body){
		if(err) return callback(err);
		var results = body.split('\n');
		var output = {};
		for(var i=0;i<results.length;i++){
			var temp = results[i].split('\t');
			if(temp[0] != '' && temp[0] != 'num'){
				var key = decodeURIComponent(temp[0].substring(1));
				output[key] = decodeURIComponent(temp[1]);
			} else continue; 
		}
		return callback('',output);
	});	
}

Kyoto.setBulk = setBulk;

function setBulk(data,db,callback){
	if(!callback) callback = function(){};
	var url = URL.format(uri)+'/rpc/set_bulk';
	var content = 'atomic\t\n';
	content += 'DB\t'+db+'\n';
	// encode all key - value
	for(var x in data){
		content += '_'+encodeURIComponent(x)+'\t'+encodeURIComponent(data[x])+'\n';
	}
	request({
		method:'POST',
		uri:url,
		headers:{
			'content-type': 'text/tab-separated-values; colenc=U'
		},
		body:content
	},function(err,res,body){
		if(err) return callback(err);
		return callback('',body);
	});		
}

Kyoto.remove = function(key,db,callback){
	if(!callback) callback = function(){};
	var url = URL.format(uri)+'/rpc/remove';
	var content = 'DB\t'+db+'\n';
	content += 'key\t'+encodeURIComponent(key)+'\n';
	request({
		method:'POST',
		uri:url,
		headers:{
			'content-type': 'text/tab-separated-values; colenc=U'
		},
		body:content
	},function(err,res,body){
		if(err) return callback(err);
		return callback('',body);
	});		
}

Kyoto.removeBulk = function(keys,db,callback){
	if(!callback) callback = function(){};
	var url = URL.format(uri)+'/rpc/remove_bulk';
	var content = 'atomic\t\n';
	content += 'DB\t'+db+'\n';
	for(var i=0;i<keys.length;i++){
		content += '_'+encodeURIComponent(keys[i])+'\t\n';
	}
	request({
		method:'POST',
		uri:url,
		headers:{
			'content-type': 'text/tab-separated-values; colenc=U'
		},
		body:content
	},function(err,res,body){
		if(err) return callback(err);
		return callback('',body);
	});	
}

Kyoto.append = function(key,value,db,callback){
	if(!callback) callback = function(){};
	var url = URL.format(uri)+'/rpc/append';
	var content = 'DB\t'+db+'\n';
	content += 'key\t'+encodeURIComponent(key)+'\n';
	content += 'value\t'+encodeURIComponent(','+value)+'\n';
	request({
		method:'POST',
		uri:url,
		headers:{
			'content-type': 'text/tab-separated-values; colenc=U'
		},
		body:content
	},function(err,res,body){
		if(err) return callback(err);
		return callback('',body);
	});		
}

Kyoto.clear = clear_partition;
function clear_partition(partition_num,callback){
	if(!callback) callback = function(){};
	var url = URL.format(uri)+'/rpc/clear';
	var content = 'DB\t'+'part_'+partition_num+'.kch'+'\n';	
	request({
		method:'POST',
		uri:url,
		headers:{
			'content-type': 'text/tab-separated-values; colenc=U'
		},
		body:content
	},function(err,res,body){
		if(err) return callback(err);
		Record.Partition.clearPartition(partition_num,function(err2,results2){
			if(err2) return callback(err2);
			return callback('',body);
		});
	});		
}

Kyoto.partition = function(data,callback){
	if(!callback) callback = function(){};
	// split partition
	var length = Object.keys(data).length;
	var index = 0;
	partition(index,length,data,callback);
}

// perform patitioning
function partition(index,length,data,callback){
	if(index>=length) return callback('','');
	if(!callback) callback = function(){};
	// get all data key
	var keys = Object.keys(data[index]);
	// get keys
	Record.Partition.getKeys(index,function(err,results){
		if(err) return callback(err);
		var existed_keys = results;
		var get_keys = [];
		var new_keys = [];
		// first 
		if(existed_keys.length==0){
			new_keys = keys;
			get_keys = [];
		} else{
			get_keys = intersect(keys,existed_keys)
			new_keys = difference(keys,existed_keys);
		} 

		if(get_keys.length>0){
			// get data first
			getBulk(get_keys,'part_'+index+'.kch',function(err2,results2){
				if(err2) return callback(err2);
				var datas = results2;
				var part = data[index];
				for(var key in part){
					if(!datas[key]) datas[key] = part[key];
					else datas[key] += ','+part[key];
				}
				setBulk(datas,'part_'+index+'.kch',function(err3,results3){
					if(err3) return callback(err3);
					Record.Partition.setKeys(new_keys,index,function(err4,results4){
						if(err4) return callback(err4);
						index++;
						partition(index,length,data,callback);
					});					
				});
			});
		} else {
			// set bulk
			var datas = {};
			var part = data[index];
			for(var key in part){
				datas[key] = part[key];
			}
			setBulk(datas,'part_'+index+'.kch',function(err2,results2){
				if(err2) return callback(err2);
				Record.Partition.setKeys(new_keys,index,function(err3,results3){
					if(err3) return callback(err3);
					index++;
					partition(index,length,data,callback);
				});
			});
		}
	});
}

Kyoto.writePartition = function(output_path,partition_number,callback){
	if(!callback) callback = function(){};
	var total = partition_number;
	var index = 0;
	return partition_write(index,total,output_path,callback);
}

function partition_write(index,total,output_path,callback){
	if(index >= total) return callback('');
	// get key attributes
	Record.Partition.getKeys(index,function(err,results){
		if(err) return callback(err);
		var keys = results;
		getBulk(keys,'part_'+index+'.kch',function(err2,results2){
			if(err2) return callback(err2);
			var final_output = '';
			for(var key in results2){
				final_output += key+'\t'+results2[key]+'\n';
			}
			fs.writeFile(output_path+'part_'+index,final_output,function(errFs){
				if(errFs) return callback(errFs);
				// clear partition database
				clear_partition(index,function(err3,results3){
					if(err3) return callback(err3);
					index++;
					return partition_write(index,total,output_path,callback);
				});
			});
		});
	});
}

// finds the intersection of two arrays
function intersect(a,b){
	// sort array first
	a = a.sort();
	b = b.sort();
	// begin
	var ai = 0;
	var bi = 0;
	var result = [];
	while(ai<a.length && bi<b.length){
		if(a[ai] < b[bi]) ai++;
		else if(a[ai] > b[bi]) bi++;
		else{
			result.push(a[ai]);
			ai++;
			bi++;
		}
	}
	return result;
}

// difference
function difference(a,b){
	var result = [];
	var temp = {};
	for(var i=0;i<b.length;i++){
		temp[b[i]] = 1;
	}
	for(var i=0;i<a.length;i++){
		if(!temp[a[i]]) result.push(a[i]);
	}
	return result;
}