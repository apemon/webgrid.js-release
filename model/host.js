exports.host = function(option){
	if(!option) option = {};
	this.id = option.id;
	this.authen = option.authen || random_word(32);
	this.platform = option.platform;
	this.last_ip_addr = option.last_ip_addr;
	if(option.host_info){
		this.version = option.host_info.browser_info.version;
		this.timezone_offset = option.host_info.browser_info.timezone_offset;
		this.os_platform = option.host_info.browser_info.platform;
		this.flops = option.host_info.flops;
		this.user_agent = option.host_info.browser_info.user_agent;
	} else {
		this.version = option.version;
		this.timezone_offset = option.timezone_offset;
		this.os_platform = option.os_platform;
		this.flops = option.flops;
		this.user_agent = option.user_agent;
	}
	var current_time;
	if(!option.create_time) current_time = new Date().getTime();
	this.create_time = option.create_time || current_time;
	this.expire_time = option.expire_time || current_time + (21*86400000); // 3 week
	this.last_mod_time = option.last_mod_time || current_time;
	this.last_active_time = option.last_active_time || current_time;
}

var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
function random_word(length){
	var text="";
	for(var i=0;i<length;i++){
		text += possible.charAt(Math.floor(Math.random() * possible.length));
	}
	return text;
}