exports.task = function(option){
	if(!option) option = {};
	this.id = option.id;
	this.status = option.status || 'create';
	var current_time = new Date().getTime();
	this.create_time = option.create_time || current_time;
	this.validate_time = option.validate_time || 0;
	this.file_write_time = option.file_write_time || 0;
	this.partition_time = option.partition_time || 0;
	this.complete_time = option.complete_time || 0;
	this.last_mod_time = option.last_mod_time || current_time;
	this.job_id = option.job_id || 0;
	this.candidate_result = option.candidate_result || 0;
	this.input_path = option.input_path || '';
	this.output_checksum = option.output_checksum || ''; 
	this.output_path = option.output_path || '';
}