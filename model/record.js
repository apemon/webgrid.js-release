var mysql = require('mysql'),
	crypto = require('crypto'),
	fs = require('fs');

// Model
var task = require('./task').task,
	result = require('./result').result,
	host = require('./host').host;
// 
var Task = {};
var client;
var info = {};
var clients = [];
var clientPool_size = 0;
var Record = {};

var index = 0;
//var Record = module.exports = {};

module.exports = Record;

Record.Job = {};
Record.Task = {};
Record.Result = {};
Record.Workflow = {};
Record.Host = {};
Record.Platform = {};
Record.Configuration = {};
Record.Mount = {};
Record.Partition = {};

Record.init = function(mysql_info){
	info['host'] = mysql_info.host;
	info['user'] = mysql_info.username;
	info['passwd'] = mysql_info.password;
	info['database'] = mysql_info.dbname;
}

function connect(){
	client = mysql.createClient({
		host:info.host,
		user:info.user,
		password:info.passwd,
		database:info.database
	});
}

function newClient(){
	var new_client = mysql.createClient({
		host:info.host,
		user:info.user,
		password:info.passwd,
		database:info.database
	});
	return new_client;
}

function disconnect(){
	client.end();
}

Record.setClientPool = function(num){
	clientPool_size = num;
}

Record.connect = function(){
	if(clientPool_size==0) return connect();
	
	clients = [];
	for(var i=0;i<clientPool_size;i++){
		var new_client = newClient();
		clients.push(new_client);
	}
}

Record.disconnect = function(){
	if(clients.length>0){
		for(var i=0;i<clients.length;i++){
			clients[i].end();
		}
		clients = [];
	}
}

Record.reset = function(callback){
	var index = 0;
	var total = 7;
	var multi_results = {};
	var tbname = [];
	tbname.push('job');
	tbname.push('task');
	tbname.push('result');
	tbname.push('workflow');
	tbname.push('platform');
	tbname.push('host');
	tbname.push('configuration');
	tbname.push('partition');
	connect();
	for(var i=0;i<total;i++){
		truncate(tbname[i],function(err,results){
			if(err){ 
				disconnect();
				return callback(err);
			}
			index++;
			multi_results[results.name] = results.data;
			if(index>=total) {
				disconnect();
				return callback('',multi_results);
			}
		});
	}
}

function truncate(table,callback){
	client.query('truncate table '+table,function(err,results){
		if(err) return callback(err);
		var re = {};
		re['name'] = table;
		re['data'] = results;
		return callback('',re);
	});
}

Record.query = function(sql,callback){
	if(!callback) callback=function(){};
	connect();
	client.query(sql,function(err,results){
		disconnect();
		if(err) return callback(err);
		return callback('',results);
	});
}

/*
	table (string) - table name
	fields (array) - list of query fields, if null array, it mean select all fields
	conds (string) - conditional string
	callback (function) - callback function
*/
Record.select = select;

function select(table,fields,conds,callback){
	if(!callback) callback=function(){};
	connect();
	var field_string = '';
	if(fields.length==0) field_string = '*';
	else field_string = fields.join();
	if(conds.length>0) conds = ' where ' + conds
	
	var sql = 'select '+field_string+' from '+table+conds;
	client.query(sql,function(err,results){
		disconnect();
		if(err) return callback(err);
		return callback('',results);
	});
}

Record.Platform.list = function(callback){
	if(!callback) callback=function(){};
	connect();
	client.query('select * from platform',function(err,results){
		disconnect();
		if(err) return callback(err);
		return callback('',results);
	});			
}

Record.Platform.getbyId = function(id,callback){
	if(!callback) callback=function(){};
	connect();
	client.query('select * from platform where id='+id,function(err,results){
		disconnect();
		if(err) return callback(err);
		return callback('',results);
	});		
}

Record.Platform.getbyName = platform_getbyName;

function platform_getbyName(name,callback){
	if(!callback) callback=function(){};
	connect();
	client.query('select * from platform where name="'+name+'"',function(err,results){
		disconnect();
		if(err) return callback(err);
		return callback('',results);
	});		
}

Record.Platform.getbyVendor = platform_getbyVendor;

function platform_getbyVendor(vendor,callback){
	if(!callback) callback=function(){};
	connect();
	client.query('select * from platform where vendor="'+vendor+'"',function(err,results){
		disconnect();
		if(err) return callback(err);
		return callback('',results);
	});		
}

Record.Platform.batchInsert = platform_batchInsert;

function platform_batchInsert(platforms,callback){
	if(!callback) callback=function(){};
	connect();
	var values = [];
	for(var i=0;i<platforms.length;i++){
		var value = '(NULL';
				value += ',"'+platforms[i].name+'"';
				value += ',"'+platforms[i].vendor+'"';
				value += ',"'+platforms[i].description+'"';
				value += ','+new Date().getTime()+')';
		values.push(value);
	}
	var sql = "insert into platform values "+values.join();
	client.query(sql,function(err,results){
		disconnect();
		if(err) return callback(err);
		return callback('',results);
	});
}

Record.Host.list = function(callback){
	if(!callback) callback=function(){};
	connect();
	client.query('select * from host',function(err,results){
		disconnect();
		if(err) return callback(err);
		return callback('',results);
	});		
}

Record.Host.getbyId = function(id,callback){
	if(!callback) callback=function(){};
	connect();
	client.query('select * from host where id='+id,function(err,results){
		disconnect();
		if(err) return callback(err);
		return callback('',results);
	});	
}

Record.Host.getbyAuthen = function(authen,callback){
	if(!callback) callback=function(){};
	connect();
	client.query('select * from host where authen="'+authen+'"',function(err,results){
		disconnect();
		if(err) return callback(err);
		return callback('',results);
	});	
}

Record.Host.authen = host_authen;

function host_authen(id,authen,callback){
	if(!callback) callback=function(){};
	connect();
	var sql = 'select id,create_time,expire_time from host where id="'+id+'" and authen="'+authen+'"';
	client.query(sql,function(err,results){
		// check for expire time, if expire, create new authen
		//var current_time = new Date().getTime();
		disconnect();
		if(err) return callback(err);
		return callback('',results);
	});		
}

Record.Host.register = host_register;

// host_info is the same host_info object in client-side webgrid.js
function host_register(host_info,ip_addr,callback){
	if(!callback) callback=function(){};
	connect();
	// query to group platform
	var browser_info = host_info.browser_info;
	client.query('select id from platform where name="'+browser_info.browser+'"',function(err,results){
		if(err){
			disconnect();
			return callback(err);
		}
		var plat_id = results[0].id;
		// create new host and insert data to db
		var options = {};
		options['host_info'] = host_info;
		options['platform'] = plat_id;
		options['last_ip_addr'] = ip_addr;
		var new_host = new host(options);
		var values = '(NULL'; // id
				values += ',"'+new_host.authen+'"'; //authen
				values += ','+new_host.platform; // platform
				values += ',"'+new_host.version+'"'; // version 
				values += ','+new_host.timezone_offset; // timezone_offset
				values += ',"'+new_host.last_ip_addr+'"'; // last_ip_addr
				values += ','+new_host.create_time; // create_time
				values += ','+new_host.expire_time; // expire_time
				values += ','+new_host.last_mod_time; // last_mod_time
				values += ','+new_host.last_active_time; //last_active_time
				values += ',"'+new_host.os_platform+'"'; // os_platform
				values += ','+new_host.flops; // flops
				values += ',"'+new_host.user_agent+'")'; // user_agent
		client.query('insert into host values'+values,function(err2,results2){
			disconnect();
			if(err2) return callback(err2);
			var output = {};
			output['id'] = results2.insertId;
			output['authen'] = new_host.authen;
			output['platform'] = new_host.platform;
			return callback('',output);
		});
	});
}

Record.Host.updatebyId = host_updatebyId;

function host_updatebyId(id,update_host,callback){
	if(!callback) callback=function(){};
	var update_statement = [];
	for(var x in update_host){
		var temp = x +'=';
		if(typeof(update_host[x])=='string') temp+='"'+update_host[x]+'"';
		else if(typeof(update_host[x])=='number') temp+=update_host[x];
		else return callback('invalid format');
		update_statement.push(temp);
	}
	var sql = 'update host set '+update_statement.join()+' where id='+id;
	connect();
	client.query(sql,function(err,results){
		disconnect();
		if(err) return callback(err);
		return callback('',results);
	});
}

Record.Host.updateActiveTime = function(id,ip_addr,callback){
	if(!callback) callback=function(){};
	var update_host = {};
	update_host['last_active_time'] = new Date().getTime();
	update_host['last_ip_addr'] = ip_addr;
	host_updatebyId(id,update_host,callback);
}

Record.Host.prolong = function(id,old_authen,host_info,ip_addr,callback){
	if(!callback) callback=function(){};
	// query to group platform
	var browser_info = host_info.browser_info;
	connect();
	client.query('select id from platform where name="'+browser_info.browser+'"',function(err,results){
		if(err){
			disconnect();
			return callback(err);
		}
		var plat_id = results[0].id;
		var options = {};
		options['host_info'] = host_info;
		options['platform'] = plat_id;
		options['last_ip_addr'] = ip_addr;
		var update_host = new host(options);
		// sql statement
		var update_statement = [];
		for(var x in update_host){
			if(x=='id' || x=='create_time') continue;
			var temp = x+'=';
			if(typeof(x)=='string') temp+='"'+update_host[x]+'"';
			else if(typeof(update_host[x])=='number') temp+=update_host[x];
			else return callback('invalid format');
			update_statement.push(temp);
		}
		var sql = 'update host set '+update_statement.join()+' where id='+id;
		client.query(sql,function(err2,results){
			disconnect();
			if(err2) return callback(err2);
			var output = {};
			output['id'] = id;
			output['authen'] = update_host.authen;
			output['platform'] = update_host.platform;
			return callback('',output);
		});
	});
}

Record.Host.getReservedResult = function(host_id,jobId,callback){
	if(!callback) callback = function(){};
	connect();
	var current_time = new Date().getTime();
	var query_string = 'select r.id,r.deadline_time,r.host_id,r.task_id,r.secret,t.input_path from result r left join task t on r.task_id=t.id where r.job_id='+jobId+' and r.host_id='+host_id+' and r.deadline_time>'+current_time+' and r.status="create"';
	client.query(query_string,function(err,results){
		disconnect();
		if(err) return callback(err);
		return callback('',results);
	});
}

Record.Host.reserveResult = function(host_id,result_id,deadline_duration,dispatch_time,callback){
	if(!callback) callback = function(){};
	connect();
	var current_time = new Date().getTime();
	var deadline_time = current_time + deadline_duration;
	var query_string = 'update result set host_id='+host_id+',deadline_time='+deadline_time+',dispatch_time='+dispatch_time+' where id='+result_id;
	client.query(query_string,function(err,results){
		disconnect();
		if(err) return callback(err);
		results['deadline_time'] = deadline_time;
		return callback('',results);
	});
}

Record.Job.insert = job_create;

Record.Job.list = function(callback){
	if(!callback) callback=function(){};
	connect();
	client.query('select * from job',function(err,results){
		disconnect();
		if(err) return callback(err);
		return callback('',results);
	});	
}

Record.Job.getbyId = job_getbyId; 

function job_getbyId(id,callback){
	if(!callback) callback=function(){};
	connect();
	client.query('select * from job where id='+id,function(err,results){
		disconnect();
		if(err) return callback(err);
		return callback('',results);
	});
}

Record.Job.getbyName = job_getbyName;

function job_getbyName(name,callback){
	if(!callback) callback = function(){};
	connect();
	client.query('select * from job where name="'+name+'"',function(err,results){
		disconnect();
		if(err) return callback(err);
		return callback('',results);
	});
}

Record.Job.getbyStatus = job_getbyStatus;

function job_getbyStatus(status,fields,callback){
	if(!callback) callback=function(){};

	var select_field;
	if(typeof(fields) == 'string'){
		if(fields == '' || fields == '*') select_field = '*';
		else select_field = fields;
	} else {
		select_field = fields.join(',');
	}

	connect();
	client.query('select '+select_field+' from job where status="'+status+'"',function(err,results){
		disconnect();
		if(err) return callback(err);
		return callback('',results);
	});	
}

Record.Job.changeStatus = function(jobId,status,callback){
	if(!callback) callback = function(){};
	connect();
	var current_time = new Date().getTime();
	client.query('update job set status="'+status+'",last_mod_time='+current_time+' where id='+jobId,function(err,results){
		disconnect();
		if(err) return callback(err);
		return callback('',results);
	});
}

Record.Job.changeStatusbyJobName = function(jobName,status,callback){
	if(!callback) callback = function(){};
	connect();
	var current_time = new Date().getTime();
	client.query('update job set status="'+status+'",last_mod_time='+current_time+' where name="'+jobName+'"',function(err,results){
		disconnect();
		if(err) return callback(err);
		return callback('',results);
	});	
}

Record.Job.start = function(jobName,callback){
	if(!callback) callback = function(){};
	connect();
	var current_time = new Date().getTime();
	client.query('select * from job where name="'+jobName+'"',function(err,results){
		if(err) return callback(err);
		var job = results[0];
		client.query('update job set status="start",last_mod_time='+current_time+' where name="'+jobName+'"',function(err2,results2){
			disconnect();
			if(err2) return callback(err2);
			// create program script
			var program_script = '';
			var program_filepath = '';
			if(job.type == 'mapreduce'){
				program_script += unescape(job.input_reader)+'\n';
				program_script += unescape(job.algorithm)+'\n';
				program_script += unescape(job.combiner)+'\n';
				program_filepath += '../client/mapreduce.js';
			} else {
				program_script += unescape(job.custom);
				program_filepath += '../client/custom.js';
			}
			fs.writeFileSync(program_filepath,program_script);
			return callback('',results2);
		});		
	});

}

Record.Job.countTotalTask = job_countTotalTask;

function job_countTotalTask(jobId,callback){
	if(!callback) callback = function(){};

	connect();
	client.query('select count(id) as count from task where job_id='+jobId,function(err,results){
		disconnect();
		if(err) return callback(err);
		return callback('',results);
	});
}

Record.Job.countTaskbyStatus = job_countTaskbyStatus;

function job_countTaskbyStatus(jobId,status,callback){
	if(!callback) callback = function(){};

	connect();
	client.query('select count(id) as count from task where job_id='+jobId+' and status="'+status+'"',function(err,results){
		disconnect();
		if(err) return callback(err);
		return callback('',results);
	});
}

Record.Job.countTask = function(jobId,callback){
	if(!callback) callback = function(){};

	connect();
	client.query('select status,count(id) as count from task where job_id='+jobId+' group by status',function(err,results){
		disconnect();
		if(err) return callback(err);
		var out = {};
		for(var i=0;i<results.length;i++){
			out[results[i].status] = results[i].count;
		}
		return callback('',out);
	});
}

Record.Job.finish = function(jobId,callback){
	if(!callback) callback = function(){};

	connect();
	var complete_time = new Date().getTime();

	client.query('update job set status="complete",complete_time='+complete_time+' where id='+jobId,function(err,results){
		if(err) return callback(err);
		return callback('',results);
	});
}

/*Record.Job.collect = job_collect;

function job_collect(job,callback){
	if(!callback) callback = function(){};

	task_getByJobIdStatus(job.id,'validate',function(err,results){
		if(err) return callback(err);
		var tasks = results;

		//result_getCandidate(tasks,callback);
	});
}*/

Record.Task.batchCreate = batch_create_task;

function job_create(job,callback){
	if(!callback) callback=function(){};
	connect();
	var values = 'NULL,"'; // id
	values = values+job.name+'","'; // name
	values = values+job.description+'","'; // description
	values = values+job.type+'","';
	values = values+job.status+'",'; // status
	values = values+job.create_time+','; // create_time
	values = values+job.last_mod_time+','; // last_mod_time
	if(job.complete_time) values = values+job.complete_time+',"'; // complete_time
	else values = values+'0,"';
	values = values+job.input_directory+'","'; // input_directory
	values = values+job.output_directory+'","'; // output_directory
	values = values+job.sort_method+'","'; // sort_method
	values = values+job.output_type+'",'; // output_type
	values = values+job.partition_number+',"'; // partition_number
	values = values+job.validation_type+'",'; // validation_type
	values = values+job.error_rate+','; // error_rate
	values = values+job.max_result+','; // max_result
	values = values+job.majority+','; // majority
	values = values+job.checkpoint+','; // checkpoint
	values = values+job.checkpoint_interval+','; // checkpoint_interval
	values = values+job.deadline+','; // deadline
	values = values+job.deadline_duration+',"'; // deadline_duration
	values = values+escape(job.input_reader)+'","'; // input_reader
	values = values+escape(job.algorithm)+'","'; // algorithm
	values = values+escape(job.combiner)+'","'; // combiner
	values = values+escape(job.custom)+'"';
	client.query('insert into job values('+values+')',function(err,results){
		disconnect();
		if(err) callback(err);
		return callback('',results);
	});
}

function batch_create_task(job,callback){
	if(!callback) callback = function(){};

	mount_getPath(job.input_directory,function(err,results){
		if(err) return callback(err);
		if(results.length == 0){
			var msg = {};
			msg['status'] = 'warning';
			msg['data'] = 'path not found, make sure you set mount properly';
			return callback('',msg);
		}
		var input_directory = results;
		fs.readdir(input_directory.physical_path, function(err2,files){
			if(err2) return callback(err2);

			var multi_values = [];
			connect();
			//build mysql statement command
			for(var i=0;i<files.length;i++){
				var new_task = new task();
				new_task['input_path'] = files[i];
				var values = '(NULL,"'; // id
				values += new_task.status+'",'; //status
				values += new_task.create_time+','; // create_time
				values += new_task.validate_time+','; // validate_time
				values += new_task.file_write_time+','; // file_write_time
				values += new_task.partition_time+','; // partition_time
				values += new_task.complete_time+','; // complete_time
				values += new_task.last_mod_time+','; // last_mod_time
				values += job.id+','; // job_id
				values += new_task.candidate_result+',"' // candidate_result
				values += new_task.input_path+'","'; // input_path
				values += new_task.output_checksum+'","'; // output_checksum
				values += new_task.output_path+'")'; // output_path
				multi_values.push(values);
			}
			var sql = 'insert into task values'+multi_values.join(',');
			client.query(sql,function(err3,results3){
				disconnect();

				if(err3) return callback(err3);
				return callback('',results3);
			});
		});
	});
}

Record.Task.getbyId = task_getbyId;

function task_getbyId(id,callback){
	if(!callback) callback = function(){};
	connect();
	client.query('select * from task where id='+id,function(err,results){
		disconnect();
		if(err) return callback(err);
		return callback('',results);
	});	
}

Record.Task.getbyJobId = task_getbyJobId;

function task_getbyJobId(job_id,callback){
	if(!callback) callback = function(){};
	connect();
	client.query('select * from task where job_id='+job_id,function(err,results){
		disconnect();
		if(err) return callback(err);
		return callback('',results);
	});
}

Record.Task.rangeQuerybyJobId = task_rangeQuerybyJobId;

function task_rangeQuerybyJobId(id,begin,num,callback){
	if(!callback) callback = function(){};
	connect();
	client.query('select * from task where job_id='+id+' limit '+begin+','+num,function(err,results){
		disconnect();
		if(err) return callback(err);
		return callback('',results);
	});
}

Record.Task.rangeQuerybyJobIdStatus = task_rangeQuerybyJobIdStatus;

function task_rangeQuerybyJobIdStatus(id,status,fields,begin,num,callback){
	if(!callback) callback = function(){};

	var select_field;
	if(typeof(fields) == 'string'){
		if(fields == '' || fields == '*') select_field = '*';
		else select_field = fields;
	} else {
		select_field = fields.join(',');
	}

	var select_status;
	var has_status = false;
	if(typeof(status) == 'string'){
		if(status != ''){
			has_status = true;
			select_status = 'status = "'+status+'"';
		} 
	} else {
		has_status = true;
		var temp_status = [];
		for(var i=0;i<status.length;i++){
			temp_status.push('status ="'+status[i]+'"');
		}
		select_status = temp_status.join(' or ');
	}

	var sql;
	if(has_status) sql = 'select '+select_field+' from task where '+select_status+' and job_id='+id+' limit '+begin+','+num;
	else sql = 'select '+select_field+' from task where job_id='+id+' limit '+begin+','+num;
	connect();
	client.query(sql,function(err,results){
		disconnect();
		if(err) return callback(err);
		return callback('',results);
	});
}

Record.Task.getByJobIdStatus = task_getByJobIdStatus;

function task_getByJobIdStatus(jobId,status,callback){
	if(!callback) callback = function(){};

	connect();
	client.query('select * from task where job_id='+jobId+' and status="'+status+'"',function(err,results){
		disconnect();
		if(err) return callback(err);
		return callback('',results);
	});
}

Record.Task.updateValidateTaskId = task_updateValidateTaskId;

function task_updateValidateTaskId(taskId,acceptResultIdList,rejectResultIdList,callback){
	if(!callback) callback = function(){};

	connect();
	// update accept resultIdList
	result_updateStatusBatch(0,acceptResultIdList,'accept',function(err2,results2){
		if(err2){
			disconnect();
			return callback(err2);
		}

		result_updateStatusBatch(0,rejectResultIdList,'reject',function(err3,results3){
			if(err3){
				disconnect();
				return callback(err3);
			}

			var query_string = 'update task set status="validated",validate_time='+new Date().getTime();
			query_string += ',candidate_result='+acceptResultIdList[0]; //
			query_string += ' where id='+taskId;
			client.query(query_string,function(err4,results4){
				disconnect();
				if(err4)return callback(err4);
				return callback('',results4);
			});

		});

	});
}

function result_updateStatusBatch(index,resultIdList,status,callback){
	if(resultIdList.length==0 || index==resultIdList.length) return callback('','{"status":"success","data":"update result batch complete"}');

	client.query('update result set status="'+status+'" where id='+resultIdList[index],function(err,results){
		if(err)return callback(err);

		index++;
		return result_updateStatusBatch(index,resultIdList,status,callback);
	});
}

Record.Task.bury = function(task_id,callback){
	if(!callback) callback = function(){};

	result_updateStatusbyTaskId(task_id,'reject',function(err,results){
		if(err) return callback(err);
		task_updateStatusTaskId(task_id,'buried',function(err2,results2){
			if(err2) return callback(err2);
			return callback('',results2);
		});
	});
}

Record.Task.updateStatusTaskId = task_updateStatusTaskId; 

function task_updateStatusTaskId(task_id,status,callback){
	if(!callback) callback = function(){};

	connect();
	client.query('update task set status="'+status+'" where id='+task_id,function(err,results){
		disconnect();

		if(err) return callback(err);
		return callback('',results);
	});
}

Record.Task.partition = function(task_id,callback){
	if(!callback) callback = function(){};

	var current_time = new Date().getTime();
	var query_string = 'update task set status="complete",partition_time='+current_time+',complete_time='+current_time+' where id='+task_id;
	connect();
	client.query(query_string,function(err,results){
		disconnect();
		
		if(err) return callback(err);
		return callback('',results);
	});
}

Record.Task.complete = function(taskId,output_path,output_checksum,file_write_time,callback){
	if(!callback) callback = function(){};

	var current_time = new Date().getTime();
	var query_string = 'update task set status="complete",output_path="'+output_path+'",output_checksum="'+output_checksum+'",file_write_time='+file_write_time+',complete_time='+current_time;
	query_string += ' where id='+taskId;
	connect();
	client.query(query_string,function(err,results){
		disconnect();

		if(err) return callback(err);
		return callback('',results);
	});
}

Record.Task.createWork = function(jobName,input,number,callback){
	if(!callback) callback = function(){};
	job_getbyName(jobName,function(err,results){
		if(err) return callback(err);
		var job = results[0];
		var current_time = new Date().getTime();
		var values = [];
		var value = '(NULL,"create",'+current_time+',0,0,0,0,'+current_time+','+job.id+',0,"'+input+'","","")';
		for(var i=0;i<number;i++){
			values.push(value);
		}
		var query_string = 'insert into task values'+values.join(',');
		connect();
		client.query(query_string,function(err2,results2){
			if(err2) return callback(err2);
			// create result
			// this is tricky
			query_string = 'select id from task where input_path="'+input+'" and create_time='+current_time+' and status="create"';
			client.query(query_string,function(err3,results3){
				if(err3) return callback(err3);
				var taskList = [];
				for(var i=0;i<results3.length;i++){
					taskList.push(results3[i].id);
				}
				values = [];
				for(var i=0;i<taskList.length;i++){
					for(var j=0;j<job.majority;j++){
						var new_result = new result();
						new_result.task_id = taskList[i];
						new_result.job_id = job.id;
						value = '(NULL,"'; // id
						value += new_result.status+'",'; // status
						value += new_result.create_time+','; // create_time
						value += new_result.dispatch_time+','; // dispatch_time
						value += new_result.execution_time+','; // execution_time
						value += new_result.receive_time+','; // receive_time
						value += new_result.client_send_time+','; // client_send_time
						value += new_result.client_receive_time+',' //client_receive_time
						value += new_result.deadline_time+','; // deadline_time
						value += new_result.task_fetch+','; // task_fetch time
						value += new_result.input_fetch+','; // input_fetch time
						value += new_result.result_send+',"'; // result_send time
						value += new_result.secret+'","'; // secret
						value += new_result.user_agent+'","'; // user_agent
						value += new_result.remote_address+'",'; // remote_address
						value += new_result.host_id+','; // host_id
						value += new_result.task_id+','; // task_id
						value += new_result.job_id+',"'; // job_id
						value += new_result.output_path+'","' // output_path
						value += new_result.output_checksum+'")'; // output_checksum 
						values.push(value);
					}
				}
				query_string = 'insert into result values'+values.join(',');
				client.query(query_string,function(err4,results4){
					disconnect();
					if(err4) return callback(err4);
					return callback('',results4);
				});
			});
			
		});
	});
}

Record.Result.createbyJobTaskList = result_createbyJobTaskId;

function result_createbyJobTaskId(job_id,task_id,number,callback){
	if(!callback) callback = function(){};
	var multi_values = [];
	for(var i=0;i<number;i++){
		var new_result = new result();
		new_result.task_id = task_id;
		new_result.job_id = job_id;
		var values = '(NULL,"'; // id
		values += new_result.status+'",'; // status
		values += new_result.create_time+','; // create_time
		values += new_result.dispatch_time+','; // dispatch_time
		values += new_result.execution_time+','; // execution_time
		values += new_result.receive_time+','; // receive_time
		values += new_result.client_send_time+','; // client_send_time
		values += new_result.client_receive_time+',' //client_receive_time
		values += new_result.deadline_time+','; // deadline_time
		values += new_result.task_fetch+','; // task_fetch time
		values += new_result.input_fetch+','; // input_fetch time
		values += new_result.result_send+',"'; // result_send time
		values += new_result.secret+'","'; // secret
		values += new_result.user_agent+'","'; // user_agent
		values += new_result.remote_address+'",'; // remote_address
		values += new_result.host_id+','; // host_id
		values += new_result.task_id+','; // task_id
		values += new_result.job_id+',"'; // job_id
		values += new_result.output_path+'","' // output_path
		values += new_result.output_checksum+'")'; // output_checksum 
		multi_values.push(values);
	}

	connect();
	client.query('insert into result values'+multi_values.join(','),function(err,results){
		disconnect();
		if(err) return callback(err);
		return callback('',results);
	});
}

Record.Result.batchCreate = result_batch_create;

function result_batch_create(job,callback){
	if(!callback) callback = function(){};
	task_getbyJobId(job.id,function(err,results){
		if(err) return callback(err);
		var task_list = results;
		var total_task = task_list.length;
		var majority = job.majority;
		var multi_values = [];
		for(var i=0;i<total_task;i++){
			for(var j=0;j<job.majority;j++){
				var new_result = new result();
				new_result.task_id = task_list[i].id;
				new_result.job_id = job.id;
				var values = '(NULL,"'; // id
				values += new_result.status+'",'; // status
				values += new_result.create_time+','; // create_time
				values += new_result.dispatch_time+','; // dispatch_time
				values += new_result.execution_time+','; // execution_time
				values += new_result.receive_time+','; // receive_time
				values += new_result.client_send_time+','; // client_send_time
				values += new_result.client_receive_time+',' //client_receive_time
				values += new_result.deadline_time+','; // deadline_time
				values += new_result.task_fetch+','; // task_fetch time
				values += new_result.input_fetch+','; // input_fetch time
				values += new_result.result_send+',"'; // result_send time
				values += new_result.secret+'","'; // secret
				values += new_result.user_agent+'","'; // user_agent
				values += new_result.remote_address+'",'; // remote_address
				values += new_result.host_id+','; // host_id
				values += new_result.task_id+','; // task_id
				values += new_result.job_id+',"'; // job_id
				values += new_result.output_path+'","' // output_path
				values += new_result.output_checksum+'")'; // output_checksum 
				multi_values.push(values);
			}
		}

		connect();
		client.query('insert into result values'+multi_values.join(','),function(err2,results2){
			disconnect();
			if(err2) return callback(err2);
			return callback('',results2);
		});
	});
}

Record.Result.getbyTaskList = result_getbyTaskList;

function result_getbyTaskList(taskList,status,callback){
	var has_status = true;
	if(typeof(status) == 'function'){
		has_status = false;
		callback = status;
	}

	if(!callback) callback = function(){};
	
	var taskIdList = [];
	for(var i=0;i<taskList.length;i++){
		taskIdList.push(taskList[i].id);
	}

	connect();

	var query_string;
	if(has_status) query_string = 'select * from result where status="'+status+'" and task_id in ('+taskIdList.join(',')+')';
	else query_string = 'select * from result where task_id in ('+taskIdList.join(',')+')';
	
	client.query(query_string,function(err,results){
		disconnect();
		if(err) return callback(err);
		return callback('',results);
	});
}

Record.Result.getbyTaskIdList = result_getbyTaskIdList;

function result_getbyTaskIdList(taskIdList,status,fields,callback){
	if(!callback) callback = function(){};

	connect();

	var query_string;
	var select_field;
	var status_field;
	if(typeof(fields) == 'string'){
		if(fields == '*' || fields == '') select_field = '*';
		else select_field = fields;
	} else {
		select_field = fields.join(',');
	}

	var has_status = false;
	if(typeof(status) == 'string'){
		if(status != ''){
			has_status = true;
			status_field = 'status="'+status+'"';
		} 
	} else {
		has_status = true;
		var temp_status = [];
		for(var i=0;i<status.length;i++){
			temp_status.push('status="'+status[i]+'"');
		}
		status_field = temp_status.join(' or ');
	}

	var current_time = new Date().getTime()
	if(has_status) query_string = 'select '+select_field+' from result where '+status_field+' and host_id = 0 and task_id in ('+taskIdList.join(',')+')';
	else query_string = 'select '+select_field+' from result where host_id = 0 and task_id in ('+taskIdList.join(',')+')';
	
	client.query(query_string,function(err,results){
		disconnect();
		if(err) return callback(err);
		return callback('',results);
	});
}

// check for result that has due and unlock the result from that user
Record.Result.updateDeadline = function(job_id,callback){
	if(!callback) callback = function(){};

	connect();

	var current_time = new Date().getTime();
	var query_string = 'update result set host_id=0,deadline_time=0 where status="create" and deadline_time!=0 and deadline_time<'+current_time+' and job_id='+job_id;
	
	client.query(query_string,function(err,results){
		disconnect();
		if(err) return callback(err);
		return callback('',results);
	});
}

/*Record.Result.getCandidate = result_getCandidate;

function result_getCandidate(taskIdList,callback){
	if(!callback) callback = function(){};

	var query = 'select id,status';
	var multi
	for(var i=0;i<taskIdList.length;i++){

	}
	connect();

}*/

Record.Result.submit = result_submit;

function result_submit(result,path,callback){
	if(!callback) callback = function(){};

	var result_submit_restrict_fields = {'id':1,'secret':1,'deadline_time':1,'task_id':1,'job_id':1,'host_id':1}

	var output = JSON.stringify(result.output);
	delete result['output'];

	var filename = result.input_url.substring(result.input_url.lastIndexOf('/')+1);
	filename = path+result.job_id+'_'+result.task_id+'_'+result.id+'_'+filename;
	delete result['input_url'];

	// update result
	var query_string = 'update result set status="fill",execution_time='+result.execution_time+',receive_time='+result.receive_time+',client_send_time='+result.client_send_time+',client_receive_time='+result.client_receive_time+',task_fetch='+result.task_fetch+',input_fetch='+result.input_fetch+',user_agent="'+result.user_agent+'",remote_address="'+result.remote_address+'",output_path="'+filename+'"'
	query_string += ' where id='+result.id+' and secret="'+result.secret+'" and status="create"';
	connect();
	client.query(query_string,function(err,results){
		disconnect();
		if(err) return callback(err);
		// check update results
		if(results.affectedRows != 1) return callback('',results); 
		fs.writeFile(filename,output,function(errfs){
			if(errfs) return callback(errfs);
			return callback('',results);
		});
	});
}

Record.Result.removeOutput = function(taskId,callback){
	if(!callback) callback =function(){};

	connect();
	client.query('update result set output_path="",output_checksum="" where task_id='+taskId,function(err,results){
		disconnect();
		if(err) return callback(err);
		return callback('',results);
	});
}

Record.Result.update = result_update;

function result_update(result,restrict_fields,callback){
	
	var update_statement = [];

	for(var x in result){
		if(restrict_fields[x]) continue;
		if(typeof(result[x]) == 'number') update_statement.push(x+'='+result[x]);
		else update_statement.push(x+'="'+result[x]+'"');
	}

	var query_string = 'update result set '+update_statement.join(',')+' where id='+result.id+' and secret="'+result.secret+'"';

	if(index>=clientPool_size) index = 0;
	clients[index].query(query_string,function(err,results){
		index++;

		if(err) return callback(err);
		return callback('',results);
	});
}

Record.Result.updateStatus = result_updateStatus;

function result_updateStatus(resultId,status,callback){
	if(!callback) callback = function(){};

	connect();
	client.query('update result set status="'+status+'" where id='+resultId,function(err,results){
		disconnect();
		if(err) return callback(err);
		return callback('',results);
	});
}

Record.Result.updateStatusbyTaskId = result_updateStatusbyTaskId;

function result_updateStatusbyTaskId(taskId,status,callback){
	if(!callback) callback = function(){};

	connect();
	client.query('update result set status="'+status+'" where task_id='+taskId,function(err,results){
		disconnect();
		if(err) return callback(err);
		return callback('',results);
	});
}

Record.Result.getResultCount = result_countTaskId;

function result_countTaskId(jobId,status,callback){
	if(!callback) callback = function(){};

	var query_string = 'select task_id,count(id) as count from result where job_id='+jobId+' and status="'+status+'" group by task_id';
	
	connect();
	client.query(query_string,function(err,results){
		disconnect();
		if(err) return callback(err);
		return callback('',results);
	});
}

Record.Result.updateTime = function(result_time,callback){
	if(!callback) callback = function(){};
	var query_string = 'update result set result_send='+result_time.result_send+' where id='+result_time.id+' and secret="'+result_time.secret+'" and status="fill" and result_send=0';
	connect();
	client.query(query_string,function(err,results){
		disconnect();
		if(err) return callback(err);
		return callback('',results);
	});
}

Record.Result.getFillResult = result_getFillResult;

function result_getFillResult(task_id,callback){
	if(!callback) callback = function(){};

	var query_string = 'select id,output_path from result where task_id='+task_id+' and status="fill"';
	connect();
	client.query(query_string,function(err,results){
		disconnect();

		if(err) return callback(err);
		var output_data = {};
		var output_path = {};
		var path_list = [];
		for(var i=0;i<results.length;i++){
			output_path[results[i].id] = results[i].output_path;
			path_list.push(results[i].output_path);
		}

		read_files(path_list,function(err2,data){
			if(err2) return callback(err2);
			var output = {};
			for(var x in data){
				var temp = x.split('_');
				output_data[temp[2]] = data[x];
			}
			output['results'] = results;
			output['data'] = output_data
			return callback('',output);
		});
	});
}

Record.Result.getAVGAcceptResult = result_getAVGAcceptResult;

function result_getAVGAcceptResult(task_id,callback){
	if(!callback) callback = function(){};

	var query_string = 'select id,output_path from result where task_id='+task_id+' and status="accept"';
	connect();
	client.query(query_string,function(err,results){
		disconnect();

		if(err) return callback(err);
		var output_data = {};
		var output_path = {};
		var path_list = [];
		for(var i=0;i<results.length;i++){
			output_path[results[i].id] = results[i].output_path;
			path_list.push(results[i].output_path);
		}

		read_files(path_list,function(err2,data){
			if(err2) return callback(err2);
			var output = {};
			for(var x in data){
				var temp = x.split('_');
				output_data[temp[2]] = data[x];
			}
			output['results'] = results;
			output['data'] = output_data;
			return callback('',output);
		});
	});
}

Record.Result.getChecksumAcceptResult = result_getChecksumAcceptResult;

function result_getChecksumAcceptResult(task_id,callback){
	if(!callback) callback = function(){};

	var query_string = 'select id,output_path from result where task_id='+task_id+' and status="accept"';
	connect();
	client.query(query_string,function(err,results){
		disconnect();

		if(err) return callback(err);
		// since result has same data; fetch only one
		var file_path = results[0].output_path;

		read_file(file_path,function(err2,data){
			if(err2) return callback(err2);
			var output = {};
			output['results'] = results;
			output['data'] = data;
			return callback('',output);
		});
	});
} 

Record.Result.getAcceptResult = function(task_id,callback){
	if(!callback) callback = {};

	var query_string = 'select id,output_path from result where task_id='+task_id+' and status="accept" limit 1';
	connect();
	client.query(query_string,function(err,results){
		disconnect();

		if(err) return callback(err);
		// since result has same data; fetch only one
		var file_path = results[0].output_path;

		read_file(file_path,function(err2,data){
			if(err2) return callback(err2);
			var output = {};
			output['results'] = results;
			output['data'] = data;
			return callback('',output);
		});
	});	
}

Record.Result.getAcceptResults = function(task_id,callback){
	if(!callback) callback = {};

	var query_string = 'select id,output_path from result where task_id='+task_id+' and status="accept"';
	connect();
	client.query(query_string,function(err,results){
		disconnect();

		if(err) return callback(err);
		// get all accept results
		var file_path = [];
		var result_list = {};
		for(var i=0;i<results.length;i++){
			file_path.push(results[i].output_path);
			result_list[results[i].id] = results[i];
		}
		read_files(file_path,function(err2,data){
			if(err2) return callback(err2);
			var outputs = [];
			for(var x in data){
				var result_id = x.split('_');
				result_id = result_id[2];
				var output = {};
				output['data'] = data[x];
				output['result'] = result_list[result_id];
				outputs.push(output);
			}
			return callback('',outputs);
		});
	});	
}

Record.Result.getOutputPathbyTaskId = function(taskId,callback){
	if(!callback) callback = function(){};

	connect();
	client.query('select output_path from result where task_id='+taskId,function(err,results){
		disconnect();
		if(err) return callback(err);
		return callback('',results);
	});
}

Record.Workflow.connect = function(srcJobId,dstJobId,callback){
	if(!callback) callback = function(){};

	var values = '(';
	values += 'NULL,';
	values += srcJobId+',';
	values += dstJobId+')';

	connect();
	client.query('insert into workflow values'+values,function(err,results){
		disconnect();
		if(err) return callback(err);
		return callback('',results);
	});	
}

Record.Workflow.remove = function(srcJobId,dstJobId,callback){
	if(!callback) callback = function(){};

	connect();
	client.query('delete from workflow where source='+srcJobId+' and destination='+dstJobId,function(err,results){
		disconnect();
		if(err) return callback(err);
		return callback('',results);
	});	
}

Record.Workflow.getForwardFlow = workflow_getForwardFlow; 

function workflow_getForwardFlow(callback){
	if(!callback) callback = function(){};

	connect();
	client.query('select id from job',function(err,results){
		if(err){
			disconnect();
			return callback(err);
		}
		var jobId = results;
		var flow = {};
		for(var i=0;i<jobId.length;i++){
			flow[jobId[i].id] = [];
		}
		client.query('select * from workflow',function(err2,results2){
			disconnect();

			if(err2) return callback(err2);
			for(var i=0;i<results2.length;i++){
				var src = results2[i].source;
				var des = results2[i].destination
				flow[src].push(des);
			}
			return callback('',flow);
		});
	});
}

Record.Workflow.getBackwardFlow = workflow_getBackwardFlow;

function workflow_getBackwardFlow(callback){
	if(!callback) callback = function(){};

	connect();
	client.query('select id from job',function(err,results){
		if(err){
			disconnect();
			return callback(err);
		}
		var jobId = results;
		var flow = {};
		for(var i=0;i<jobId.length;i++){
			flow[jobId[i].id] = [];
		}
		client.query('select * from workflow',function(err2,results2){
			disconnect();

			if(err2) return callback(err2);
			for(var i=0;i<results2.length;i++){
				var src = results2[i].source;
				var des = results2[i].destination
				flow[des].push(src);
			}
			return callback('',flow);
		});
	});
}

Record.Workflow.getCurrentJob = function(callback){
	if(!callback) callback = function(){};

	connect();
	client.query('select * from job',function(err,results){
		if(err){
			disconnect();
			return callback(err);
		}
		var jobs = results;
		delete results;
		var jobStatus = {};
		var flow = {};
		for(var i=0;i<jobs.length;i++){
			flow[jobs[i].id] = [];
			jobStatus[jobs[i].id] = jobs[i].status;
		}
		// build backward flow
		client.query('select * from workflow',function(err2,results2){
			disconnect();

			if(err2) return callback(err2);
			// build backward workflow
			for(var i=0;i<results2.length;i++){
				var src = results2[i].source;
				var des = results2[i].destination;
				flow[des].push(src);
			}
			delete results2;
			// check condition
			var next;
			for(var x in jobStatus){
				next = false;
				if(jobStatus[x] == 'complete') continue;
				if(flow[x].length == 0){
					for(var i=0;i<jobs.length;i++){
						if(jobs[i].id == x) return callback('',jobs[i]);
					}
				} 
				var jobList = flow[x];
				for(var i=0;i<jobList.length;i++){
					if(jobStatus[jobList[i]] != 'complete'){
						next = true;
						break;
					} 
				}
				if(!next){
					for(var i=0;i<jobs.length;i++){
						if(jobs[i].id == x) return callback('',jobs[i]);
					}					
				}
			}
			return callback('',{}); // no job to run
		});
	});
}

Record.Configuration.list = function(callback){
	if(!callback) callback = function(){};

	connect();
	client.query('select * from configuration',function(err,results){
		disconnect();
		if(err) return callback(err);
		var kvList = {};
		for(var i=0;i<results.length;i++){
			kvList[results[i].name] = results[i].data;
		}
		return callback('',kvList);
	});
}

// return key-value pair
Record.Configuration.get = conf_get;

function conf_get(key,callback){
	if(!callback) callback = function(){};

	connect();
	client.query('select * from configuration where name="'+key+'"',function(err,results){
		disconnect();
		if(err) return callback(err);
		var x = {};
		x[results[0].name] = results[0].data;
		return callback(err,x);
	});	
}

Record.Configuration.set = function(key,value,callback){
	if(!callback) callback = function(){};

	connect();
	conf_set(key,value,function(err,results){
		if(err){
			disconnect();
			return callback(err);
		}
		return callback('',results);
	});
}

function conf_set(key,value,callback){
	if(!callback) callback = function(){};

	client.query('select * from configuration where name="'+key+'"',function(err,results){
		if(err){
			return callback(err);
		}

		var sql = '';
		if(results.length == 0) sql = "insert into configuration values('"+key+"','"+value+"')";
		else sql = "update configuration set data='"+value+"' where name='"+key+"'";

		client.query(sql,function(err2,results2){
			if(err2) return callback(err2);
			return callback('',results2);
		});
	});	
}	

Record.Configuration.listGet = conf_listGet;

function conf_listGet(keyList,callback){
	if(!callback) callback = function(){};

	// check for key existance
	var sql = 'select * from configuration where name in (';
	for(var i=0;i<keyList.length;i++){
		if(i==keyList.length-1) sql = sql + '"'+keyList[i]+'")';
		else sql = sql + '"'+keyList[i]+'",';
	}

	connect();
	client.query(sql,function(err,results){
		disconnect();
		
		if(err) return callback(err);
		var kvList = {}; 
		for(var i=0;i<results.length;i++){
			kvList[results[i].name] = results[i].data;
		}
		return callback('',kvList);
	});
}

Record.Configuration.listSet = function(config,callback){
	if(!callback) callback = function(){};
	
	var index = 0;
	var total = 0;
	for(var x in config){
		total++;
	}

	connect();
	for(var x in config){
		conf_set(x,config[x],function(err,results){
			if(err){
				disconnect();
				return callback(err);
			}

			index++;
			if(index>=total){
				disconnect();
				return callback('','');
			}
		});
	}
}

Record.Configuration.remove = function(key,callback){
	if(!callback) callback = function(){};

	connect();
	client.query('delete from configuration where name="'+key+'"',function(err,results){
		disconnect();
		if(err) return callback(err);
		return callback('',results);
	});	
}

Record.Configuration.clear = function(callback){
	if(!callback) callback = function(){};

	connect();
	client.query('delete from configuration',function(err,results){
		disconnect();
		if(err) return callback(err);
		return callback('',results);
	});	
}

Record.Mount.list = function(callback){
	if(!callback) callback = function(){};

	conf_get('mount',function(err,results){
		if(err) return callback(err);
		return callback('',results);
	});
}

Record.Mount.getPath = mount_getPath; 

function mount_getPath(logical_path,callback){
	if(!callback) callback = function(){};

	conf_get('mount',function(err,results){
		if(err) return callback(err);
		var mount = JSON.parse(results.mount);
		for(var i=0;i<mount.length;i++){
			if(mount[i].logical_path == logical_path) return callback('',mount[i]);
		}
		return callback('',{});
	});
}

/*
Record.Mount.setPath = function(mount,callback){
	if(!callback) callback = function(){};
}*/

Record.Partition.getKeys = function(partition_num,callback){
	if(!callback) callback = function(){};

	connect();
	client.query('select data_key from partition where partition_num='+partition_num,function(err,results){
		disconnect();
		if(err) return callback(err);
		var output = [];
		if(results.length>0){
			for(var i=0;i<results.length;i++){
				output.push(results[i]['data_key']);
			}
		}
		return callback('',output);
	});
}

Record.Partition.setKeys = function(keys,partition_num,callback){
	if(!callback) callback = function(){};

	if(keys.length == 0) return callback('','');
	connect();
	var query_string = 'insert into partition values';
	var temp_value = [];
	for(var i=0;i<keys.length;i++){
		temp_value.push('(NULL,"'+keys[i]+'",'+partition_num+')');
	}
	query_string += temp_value.join(',');
	client.query(query_string,function(err,results){
		disconnect();
		if(err) return callback(err);
		return callback('',results);
	});
}

Record.Partition.removeKey = function(key,callback){
	if(!callback) callback = function(){};

	connect();
	client.query('delete from partition where data_key="'+key+'"',function(err,results){
		disconnect();
		if(err) return callback(err);
		return callback('',results);
	});
}

Record.Partition.clearPartition = function(partition_num,callback){
	if(!callback) callback = function(){};

	connect();
	client.query('delete from partition where partition_num='+partition_num,function(err,results){
		disconnect();
		if(err) return callback(err);
		return callback('',results);
	});
}

Record.Partition.clearAll = function(callback){
	if(!callback) callback = function(){};

	connect();
	client.query('delete from partition',function(err,results){
		disconnect();
		if(err) return callback(err);
		return callback('',results);
	});
}

function read_files(files,callback){
	if(!callback) callback=function(){};

	var index = 1;
	var total = files.length;
	var cache = {};
	for(var i=0;i<total;i++){
		var file = files[i]
		read_file(file,function(err,data){
			if(err) return callback(err);
			var result_id = data.filename.substring(file.lastIndexOf('/')+1);
			cache[result_id] = data.data;
			if(index == total){
				return callback('',cache);
			} else index++;
		});
	}
}

function read_file(file,callback){
	fs.readFile(file,'utf8',function(err,data){
		if(err) callback(err);
		var output = {};
		output['filename'] = file;
		output['data'] = data;
		return callback('',output);
	});
}

function sql_timestamp(time){
	time = new Date(time);
	return time.getFullYear()+'-'+(time.getMonth()+1)+'-'+time.getDate()+' '+time.getHours()+':'+time.getMinutes()+':'+time.getSeconds();
}