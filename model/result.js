exports.result = function(option){
	if(!option) option = {};
	this.id = option.id;
	this.status = option.status || 'create';
	this.create_time = option.create_time || new Date().getTime();
	this.dispatch_time = option.dispatch || 0;
	this.receive_time = option.receive_time || 0;
	this.execution_time = option.execution_time || 0.0;
	this.client_send_time = option.client_send_time || 0;
	this.client_receive_time = option.client_receive_time || 0;
	this.deadline_time = option.deadline_time || 0;
	this.task_fetch = option.task_fetch || 0;
	this.input_fetch = option.input_fetch || 0;
	this.result_send = option.result_send || 0;
	this.secret = option.secret || random_word(32);
	this.user_agent = option.user_agent || 'unknown agent';
	this.remote_address = option.remote_address || '0.0.0.0';
	this.host_id = option.host_id || 0;
	this.task_id = option.task_id || 0;
	this.job_id = option.job_id || 0;
	this.output_path = option.output_path || '';
	this.output_checksum = option.output_checksum || '';
}

var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
function random_word(length){
	var text="";
	for(var i=0;i<length;i++){
		text += possible.charAt(Math.floor(Math.random() * possible.length));
	}
	return text;
}